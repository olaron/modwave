#[macro_export]
macro_rules! cstr {
  ($s:expr) => (
    concat!($s, "\0") as *const str as *const [i8] as *const i8
  );
}

#[macro_export]
macro_rules! offsetof {
    ($structType:ty, $field:ident) =>
    {
        &(*(0 as *const $structType)).$field as *const _ as isize
    }
}

pub const fn nullptr<T>() -> *mut T {
    std::ptr::null_mut()
}

pub const fn sizeof<T>() -> usize {
    std::mem::size_of::<T>()
}