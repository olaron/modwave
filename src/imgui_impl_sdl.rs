use crate::sdl2_usr;
use crate::imgui;
use sdl2_sys::{SDL_Window, SDL_GetPerformanceFrequency, SDL_Event, SDL_EventType};
use crate::c::nullptr;
use cimgui::ImVec2;
use std::os::raw::{c_void, c_char};

fn sdl_button(x: u32) -> u32 {
    1 << (x-1)
}

static mut WINDOW: *mut SDL_Window = nullptr();
static mut MOUSE_PRESSED: [bool; 3] = [false; 3];
static mut MOUSE_CURSORS: &'static mut [sdl2_usr::Cursor] = &mut [sdl2_usr::Cursor::new();imgui::ImGuiMouseCursor_COUNT as usize];
static mut TIME: u64 = 0;

static mut CLIPBOARD_TEXT_DATA: *mut c_char = nullptr();

unsafe extern "C" fn get_clipboard_text(_: *mut c_void) -> *const c_char
{
    if !CLIPBOARD_TEXT_DATA.is_null()
    {
        sdl2_sys::SDL_free(CLIPBOARD_TEXT_DATA as *mut c_void);
    }
    CLIPBOARD_TEXT_DATA = sdl2_sys::SDL_GetClipboardText();
    return CLIPBOARD_TEXT_DATA;
}

unsafe extern "C" fn set_clipboard_text(_: *mut c_void, text: *const c_char)
{
    sdl2_sys::SDL_SetClipboardText(text);
}

pub fn imgui_init_sdl2(window: &sdl2_usr::Window) {
    tracy_scope!();
    unsafe {
        WINDOW = window.window;
    }
    let io = imgui::get_io();
    let flags = io.backend_flags();
    *flags = (imgui::ImGuiBackendFlags_HasMouseCursors | imgui::ImGuiBackendFlags_HasSetMousePos) as i32;

    unsafe {(*io.io).BackendPlatformName = cstr!("imgui_impl_sdl");}

    io.set_keymap(cimgui::ImGuiKey_Tab, sdl2_usr::SDL_Scancode::SDL_SCANCODE_TAB as i32);
    io.set_keymap(cimgui::ImGuiKey_LeftArrow, sdl2_usr::SDL_Scancode::SDL_SCANCODE_LEFT as i32);
    io.set_keymap(cimgui::ImGuiKey_RightArrow, sdl2_usr::SDL_Scancode::SDL_SCANCODE_RIGHT as i32);
    io.set_keymap(cimgui::ImGuiKey_UpArrow, sdl2_usr::SDL_Scancode::SDL_SCANCODE_UP as i32);
    io.set_keymap(cimgui::ImGuiKey_DownArrow, sdl2_usr::SDL_Scancode::SDL_SCANCODE_DOWN as i32);
    io.set_keymap(cimgui::ImGuiKey_PageUp, sdl2_usr::SDL_Scancode::SDL_SCANCODE_PAGEUP as i32);
    io.set_keymap(cimgui::ImGuiKey_PageDown, sdl2_usr::SDL_Scancode::SDL_SCANCODE_PAGEDOWN as i32);
    io.set_keymap(cimgui::ImGuiKey_Home, sdl2_usr::SDL_Scancode::SDL_SCANCODE_HOME as i32);
    io.set_keymap(cimgui::ImGuiKey_End, sdl2_usr::SDL_Scancode::SDL_SCANCODE_END as i32);
    io.set_keymap(cimgui::ImGuiKey_Insert, sdl2_usr::SDL_Scancode::SDL_SCANCODE_INSERT as i32);
    io.set_keymap(cimgui::ImGuiKey_Delete, sdl2_usr::SDL_Scancode::SDL_SCANCODE_DELETE as i32);
    io.set_keymap(cimgui::ImGuiKey_Backspace, sdl2_usr::SDL_Scancode::SDL_SCANCODE_BACKSPACE as i32);
    io.set_keymap(cimgui::ImGuiKey_Space, sdl2_usr::SDL_Scancode::SDL_SCANCODE_SPACE as i32);
    io.set_keymap(cimgui::ImGuiKey_Enter, sdl2_usr::SDL_Scancode::SDL_SCANCODE_RETURN as i32);
    io.set_keymap(cimgui::ImGuiKey_Escape, sdl2_usr::SDL_Scancode::SDL_SCANCODE_ESCAPE as i32);
    io.set_keymap(cimgui::ImGuiKey_KeyPadEnter, sdl2_usr::SDL_Scancode::SDL_SCANCODE_KP_ENTER as i32);

    io.set_keymap(cimgui::ImGuiKey_A, sdl2_usr::SDL_Scancode::SDL_SCANCODE_A as i32);
    io.set_keymap(cimgui::ImGuiKey_C, sdl2_usr::SDL_Scancode::SDL_SCANCODE_C as i32);
    io.set_keymap(cimgui::ImGuiKey_V, sdl2_usr::SDL_Scancode::SDL_SCANCODE_V as i32);
    io.set_keymap(cimgui::ImGuiKey_X, sdl2_usr::SDL_Scancode::SDL_SCANCODE_X as i32);
    io.set_keymap(cimgui::ImGuiKey_Y, sdl2_usr::SDL_Scancode::SDL_SCANCODE_Y as i32);
    io.set_keymap(cimgui::ImGuiKey_Z, sdl2_usr::SDL_Scancode::SDL_SCANCODE_Z as i32);

    io.set_set_clipboard_text_fn(set_clipboard_text);
    io.set_get_clipboard_text_fn(get_clipboard_text);
    io.set_clipboard_user_data(std::ptr::null_mut());

    unsafe {
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_Arrow as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_ARROW);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_TextInput as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_IBEAM);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_ResizeAll as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_SIZEALL);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_ResizeNS as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_SIZENS);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_ResizeEW as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_SIZEWE);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_ResizeNESW as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_SIZENESW);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_ResizeNWSE as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_SIZENWSE);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_Hand as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_HAND);
        MOUSE_CURSORS[imgui::ImGuiMouseCursor_NotAllowed as usize] = sdl2_usr::create_system_cursor(sdl2_usr::SDL_SystemCursor::SDL_SYSTEM_CURSOR_NO);
    }
}

pub fn process_event(event: &SDL_Event) {
    tracy_scope!();
    unsafe {
        let mut io = cimgui::igGetIO().as_mut().unwrap();
        let event_type: SDL_EventType = std::mem::transmute(event.type_);
        match event_type {
            sdl2_sys::SDL_EventType::SDL_MOUSEWHEEL => {
                if event.wheel.x > 0 {io.MouseWheelH += 1.0;}
                if event.wheel.x < 0 {io.MouseWheelH -= 1.0;}
                if event.wheel.y > 0 {io.MouseWheel += 1.0;}
                if event.wheel.y < 0 {io.MouseWheel -= 1.0;}
            },
            sdl2_sys::SDL_EventType::SDL_MOUSEBUTTONDOWN => {
                if event.button.button == sdl2_sys::SDL_BUTTON_LEFT as u8 {MOUSE_PRESSED[0] = true}
                if event.button.button == sdl2_sys::SDL_BUTTON_RIGHT as u8 {MOUSE_PRESSED[1] = true}
                if event.button.button == sdl2_sys::SDL_BUTTON_MIDDLE as u8 {MOUSE_PRESSED[2] = true}
            },
            sdl2_sys::SDL_EventType::SDL_TEXTINPUT => {
                cimgui::ImGuiIO_AddInputCharactersUTF8(io, event.text.text.as_ptr());
            },
            sdl2_sys::SDL_EventType::SDL_KEYDOWN | sdl2_sys::SDL_EventType::SDL_KEYUP => {
                let key = event.key.keysym.scancode as usize;
                if !(key < io.KeysDown.len()) {
                    panic!("Unhandled scancode: {}", key);
                }
                io.KeysDown[key] = event_type == sdl2_sys::SDL_EventType::SDL_KEYDOWN;
                io.KeyShift = (sdl2_sys::SDL_GetModState() as u32 & (sdl2_sys::SDL_Keymod::KMOD_LSHIFT as u32 | sdl2_sys::SDL_Keymod::KMOD_RSHIFT as u32)) != 0;
                io.KeyCtrl = (sdl2_sys::SDL_GetModState() as u32 & (sdl2_sys::SDL_Keymod::KMOD_LCTRL as u32 | sdl2_sys::SDL_Keymod::KMOD_RCTRL as u32)) != 0;
                io.KeyAlt = (sdl2_sys::SDL_GetModState() as u32 & (sdl2_sys::SDL_Keymod::KMOD_LALT as u32 | sdl2_sys::SDL_Keymod::KMOD_RALT as u32)) != 0;
            },
            _ => {}
        }
    }
}

fn update_mouse_cursor() {
    tracy_scope!();
    unsafe {
        let io = cimgui::igGetIO();
        if (*io).ConfigFlags & cimgui::ImGuiConfigFlags_NoMouseCursorChange as i32 != 0 {
            return;
        }

        let imgui_cursor = cimgui::igGetMouseCursor();
        if (*io).MouseDrawCursor || imgui_cursor == cimgui::ImGuiMouseCursor_None
        {
            // Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
            sdl2_sys::SDL_ShowCursor(sdl2_sys::SDL_bool::SDL_FALSE as i32);
        }
        else
        {
            // Show OS mouse cursor
            sdl2_sys::SDL_SetCursor(if !MOUSE_CURSORS[imgui_cursor as usize].cursor.is_null() {MOUSE_CURSORS[imgui_cursor as usize]}else{MOUSE_CURSORS[cimgui::ImGuiMouseCursor_Arrow as usize]}.cursor);
            sdl2_sys::SDL_ShowCursor(sdl2_sys::SDL_bool::SDL_TRUE as i32);
        }
    }
}

fn update_mouse_pos_and_buttons() {
    tracy_scope!();
    unsafe {
        let io = cimgui::igGetIO();
        if (*io).WantSetMousePos {
            sdl2_sys::SDL_WarpMouseInWindow(WINDOW, (*io).MousePos.x as i32, (*io).MousePos.y as i32);
        }
        else {
            (*io).MousePos = cimgui::ImVec2{ x: f32::MIN, y: f32::MIN};
        }

        let mut mx = 0;
        let mut my = 0;
        let mouse_buttons = sdl2_sys::SDL_GetMouseState(&mut mx, &mut my);
        (*io).MouseDown[0] = MOUSE_PRESSED[0] || (mouse_buttons & sdl_button(sdl2_sys::SDL_BUTTON_LEFT)) != 0;  // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        (*io).MouseDown[1] = MOUSE_PRESSED[1] || (mouse_buttons & sdl_button(sdl2_sys::SDL_BUTTON_RIGHT)) != 0;
        (*io).MouseDown[2] = MOUSE_PRESSED[2] || (mouse_buttons & sdl_button(sdl2_sys::SDL_BUTTON_MIDDLE)) != 0;
        MOUSE_PRESSED[0] = false;
        MOUSE_PRESSED[1] = false;
        MOUSE_PRESSED[2] = false;

        if (sdl2_sys::SDL_GetWindowFlags(WINDOW) & sdl2_sys::SDL_WindowFlags::SDL_WINDOW_INPUT_FOCUS as u32) != 0 {
            (*io).MousePos = ImVec2{x: mx as f32, y: my as f32};
        }
    }
}

pub fn new_frame(window: *mut SDL_Window) {
    tracy_scope!();
    unsafe {
        let io = cimgui::igGetIO();
        if !cimgui::ImFontAtlas_IsBuilt((*io).Fonts) {
            panic!("Font atlas not built!");
        }
        let mut w = 0;
        let mut h = 0;
        let mut display_w = 0;
        let mut display_h = 0;
        sdl2_sys::SDL_GetWindowSize(window, &mut w, &mut h);
        if (sdl2_sys::SDL_GetWindowFlags(window) & (sdl2_sys::SDL_WindowFlags::SDL_WINDOW_MINIMIZED as u32)) != 0 {
            w = 0;
            h = 0;
        }
        sdl2_sys::SDL_GL_GetDrawableSize(window, &mut display_w, &mut display_h);
        (*io).DisplaySize = cimgui::ImVec2{x: w as f32,y: h as f32};
        if w > 0 && h > 0 {
            (*io).DisplayFramebufferScale = cimgui::ImVec2{x: display_w as f32 / w as f32, y: display_h as f32 / h as f32};
        }

        let frequency = SDL_GetPerformanceFrequency();
        let current_time = sdl2_sys::SDL_GetPerformanceCounter();
        (*io).DeltaTime = if TIME > 0 {
            ((current_time - TIME) as f64 / frequency as f64) as f32
        } else {
            1.0/60.0
        };
        TIME = current_time;

        update_mouse_pos_and_buttons();
        update_mouse_cursor();
    }
}