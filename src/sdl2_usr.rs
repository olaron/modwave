#![allow(dead_code)]

use sdl2_sys::*;
use std::ffi::{CString, c_void, CStr};

pub use sdl2_sys::{SDL_Scancode, SDL_SystemCursor};
use crate::c::nullptr;
use core::mem;
use std::os::raw::c_int;

fn sdl2_error() {
    unsafe {
        panic!("SDL_Error: {}\n",  std::ffi::CStr::from_ptr(SDL_GetError()).to_str().unwrap())
    }
}

pub fn gl_get_proc_address(name: &str) -> *const () {
    unsafe {
        match CString::new(name) {
            Ok(name) => SDL_GL_GetProcAddress(name.as_ptr() as *const i8) as *const (),
            Err(_) => panic!("gl_get_proc_address error")
        }
    }
}

pub fn set_swap_interval(interval: i32) {
    unsafe {
        if SDL_GL_SetSwapInterval(interval) < 0 {
            sdl2_error();
        }
    }
}

pub fn init_gl_attributes() {
    unsafe{
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_FLAGS, 0);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_PROFILE_MASK, SDL_GLprofile::SDL_GL_CONTEXT_PROFILE_CORE as i32);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MAJOR_VERSION, 3);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MINOR_VERSION, 0);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_DEPTH_SIZE, 24);
        SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_STENCIL_SIZE, 8);
    }
}

pub fn init() {
    unsafe {
        if SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0 {
            sdl2_error();
        }
    }
}

pub struct AudioDevice {
    pub device_id: u32,
    pub audio_spec: SDL_AudioSpec,
}

pub fn open_audio_device() -> AudioDevice {
    unsafe {
        let desired = SDL_AudioSpec {
            freq: 48000,
            format: AUDIO_S16 as u16,
            channels: 2,
            silence: 0,
            samples: 256,
            padding: 0,
            size: 0,
            callback: Option::None,
            userdata: nullptr()
        };
        let mut obtained = SDL_AudioSpec {
            freq: 0,
            format: 0,
            channels: 0,
            silence: 0,
            samples: 0,
            padding: 0,
            size: 0,
            callback: Option::None,
            userdata: nullptr()
        };
        let device_id = SDL_OpenAudioDevice(nullptr(), 0, &desired, &mut obtained, 0);
        if device_id == 0 {
            sdl2_error();
        }
        AudioDevice {
            device_id,
            audio_spec: obtained
        }
    }
}

impl AudioDevice {
    pub fn queue<T>(&self, data: &[T]) {
        unsafe {
            if SDL_QueueAudio(self.device_id, data.as_ptr() as *const c_void, (data.len() * mem::size_of::<T>()) as u32) != 0 {
                sdl2_error();
            }
        }
    }

    pub fn size(&self) -> u32 {
        unsafe {SDL_GetQueuedAudioSize(self.device_id)}
    }

    /// Pauses playback of the audio device.
    pub fn pause(&self) {
        unsafe { SDL_PauseAudioDevice(self.device_id, 1) }
    }

    /// Starts playback of the audio device.
    pub fn resume(&self) {
        unsafe { SDL_PauseAudioDevice(self.device_id, 0) }
    }

    pub fn status(&self) -> SDL_AudioStatus {
        unsafe {
            SDL_GetAudioDeviceStatus(self.device_id)
        }
    }
}

pub fn current_audio_driver() -> Option<&'static str> {
    unsafe {
        let buf = SDL_GetCurrentAudioDriver();
        if buf.is_null() {
            Option::None
        } else {
            Option::Some(CStr::from_ptr(buf as *const _).to_str().unwrap())
        }
    }
}

pub fn audio_playback_device_name(index: u32) -> String {
    unsafe {
        let dev_name = SDL_GetAudioDeviceName(index as c_int, 0);
        if dev_name.is_null() {
            sdl2_error();
        }
        let cstr = CStr::from_ptr(dev_name as *const _);
        cstr.to_str().unwrap().to_owned()
    }
}

pub struct Color {
    color: u32,
}

pub struct Rect {
    rect: *mut SDL_Rect,
}

impl Rect {
    pub fn null() -> Rect {
        Rect {
            rect: std::ptr::null_mut()
        }
    }
}

pub struct PixelFormat {
    format: *mut SDL_PixelFormat,
}

impl PixelFormat {
    pub fn map_rgb(&self, r: u8, g: u8, b: u8) -> Color {
        unsafe{
            let color = SDL_MapRGB(self.format, r,g,b);
            Color {
                color
            }
        }
    }
}

pub struct Surface {
    surface: *mut SDL_Surface,
}

impl Surface {
    pub fn fill_rect(&self, rect: Rect, color: Color) {
        unsafe {
            if SDL_FillRect(self.surface,rect.rect, color.color) < 0 {
                sdl2_error();
            }
        }
    }

    pub fn format(&self) -> PixelFormat {
        unsafe {
            PixelFormat {
                format: (*self.surface).format
            }
        }
    }
}

pub struct GLContext {
    context: SDL_GLContext,
}

pub struct Renderer {
    renderer: *mut SDL_Renderer,
}

impl Renderer {
    pub fn present(&self) {
        unsafe {
            SDL_RenderPresent(self.renderer);
        }
    }
}

pub struct SysWMinfo {
    pub info: SDL_SysWMinfo
}


pub struct Window {
    pub window: *mut SDL_Window,
}

impl Window {
    pub fn create() -> Window {
        unsafe {
            let window = SDL_CreateWindow(
                "modwave".as_ptr().cast(),
                SDL_WINDOWPOS_UNDEFINED_MASK as i32,
                SDL_WINDOWPOS_UNDEFINED_MASK as i32,
                1600, 900,
                SDL_WindowFlags::SDL_WINDOW_SHOWN as u32 | SDL_WindowFlags::SDL_WINDOW_OPENGL as u32 | SDL_WindowFlags::SDL_WINDOW_RESIZABLE as u32 | SDL_WindowFlags::SDL_WINDOW_ALLOW_HIGHDPI as u32);
            if window.is_null(){
                sdl2_error();
            }
            Window {
                window
            }
        }
    }

    pub fn get_surface(&self) -> Surface {
        unsafe {
            let surface = SDL_GetWindowSurface(self.window);
            if surface.is_null() {
                sdl2_error();
            }
            Surface {
                surface
            }
        }
    }

    pub fn update_surface(&self) {
        unsafe {
            if SDL_UpdateWindowSurface(self.window) < 0 {
                sdl2_error();
            }
        }
    }

    pub fn gl_create_context(&self) -> GLContext {
        unsafe {
            let context = SDL_GL_CreateContext(self.window);
            if context.is_null() {
                sdl2_error();
            }
            GLContext {
                context
            }
        }
    }

    pub fn gl_make_current(&self, context: &GLContext) {
        unsafe {
            if SDL_GL_MakeCurrent(self.window, context.context) < 0 {
                sdl2_error();
            };
        }
    }

    pub fn create_renderer(&self, index: i32, flags: u32) -> Renderer {
        unsafe {
            let renderer = SDL_CreateRenderer(self.window, index, flags);
            if renderer.is_null() {
                sdl2_error();
            }
            Renderer {
                renderer
            }
        }
    }

    pub fn get_window_wm_info(&self) -> SysWMinfo {
        unsafe {
            let mut info = SDL_SysWMinfo{
                version: SDL_version {
                    major: 0,
                    minor: 0,
                    patch: 0,
                },
                subsystem: SDL_SYSWM_TYPE::SDL_SYSWM_UNKNOWN,
                info: SDL_SysWMinfo__bindgen_ty_1 {
                    dummy: [0; 64usize]
                }
            };
            //let mut info: SDL_SysWMinfo;
            SDL_GetVersion(&mut info.version);
            let result = SDL_GetWindowWMInfo(self.window, &mut info);
            if result != SDL_bool::SDL_TRUE{
                sdl2_error();
            }
            SysWMinfo {
                info
            }
        }
    }

    pub fn get_window_display_mode(&self) -> SDL_DisplayMode {
        let mut dm = mem::MaybeUninit::uninit();

        let result = unsafe {
            SDL_GetWindowDisplayMode(
                self.window,
                dm.as_mut_ptr(),
            )
        };

        if result != 0 {
            sdl2_error();
        }

        unsafe { dm.assume_init() }
    }
}

impl Drop for Window {
    fn drop(&mut self) {
        unsafe{
            SDL_DestroyWindow(self.window);
        }
    }
}

pub fn delay(ms: u32) {
    unsafe{
        SDL_Delay(ms);
    }
}

pub fn quit() {
    unsafe{
        SDL_Quit();
    }
}

pub fn get_num_render_drivers() -> i32 {
    unsafe {
        SDL_GetNumRenderDrivers()
    }
}

pub struct RendererInfo {
    info: SDL_RendererInfo,
}

impl RendererInfo {
    pub fn name(&self) -> &str {
        unsafe {
            std::ffi::CStr::from_ptr(self.info.name).to_str().unwrap()
        }
    }
}

pub fn get_render_driver_info(index: i32) -> RendererInfo {
    unsafe {
        let mut info = SDL_RendererInfo {
            name: std::ptr::null(),
            flags: 0,
            num_texture_formats: 0,
            texture_formats: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            max_texture_width: 0,
            max_texture_height: 0
        };
        if SDL_GetRenderDriverInfo(index, &mut info) < 0 {
            sdl2_error();
        };
        RendererInfo {
            info
        }
    }
}

pub fn set_clipboard_text(text: &str) {
    unsafe {
        let text = CString::new(text).unwrap();
        if SDL_SetClipboardText(text.as_ptr()) < 0 {
            sdl2_error();
        }
    }
}

pub fn get_clipboard_text() -> String {
    unsafe {
        let clipboard = SDL_GetClipboardText();
        if clipboard.is_null() {
            sdl2_error();
        }
        CString::from_raw(clipboard).into_string().unwrap()
    }
}

#[derive(Copy, Clone)]
pub struct Cursor {
    pub cursor : *mut SDL_Cursor
}

impl Cursor {
    pub const fn new() -> Cursor {
        Cursor {
            cursor: std::ptr::null_mut()
        }
    }
}

pub fn create_system_cursor(cursor: SDL_SystemCursor) -> Cursor {
    unsafe {
        let c = SDL_CreateSystemCursor(cursor);
        if c.is_null(){
            sdl2_error()
        }
        Cursor {
            cursor: c
        }
    }
}


pub fn poll_event() -> Option<&'static SDL_Event> {
    unsafe {
        static mut EVENT: mem::MaybeUninit<sdl2_sys::SDL_Event> = mem::MaybeUninit::uninit();
        let result = SDL_PollEvent(EVENT.as_mut_ptr());
        if result == 0 {
            Option::None
        } else {
            Option::Some(&*EVENT.as_ptr())
        }
    }
}

// Utils

pub fn find_driver(name: &str) -> i32 {
    let n = get_num_render_drivers();
    let mut index = -1;
    for i in 0..n {
        let info = get_render_driver_info(i);
        if info.name().contains(name) {
            index = i;
            break;
        }
    }
    index
}

