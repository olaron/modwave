#![allow(dead_code)]

use cimgui::*;

pub use cimgui::{ImGuiBackendFlags_None,
                    ImGuiBackendFlags_HasGamepad,
                    ImGuiBackendFlags_HasMouseCursors,
                    ImGuiBackendFlags_HasSetMousePos,
                    ImGuiBackendFlags_RendererHasVtxOffset,
                    ImGuiKey_Tab,
                    ImGuiKey_LeftArrow,
                    ImGuiKey_RightArrow,
                    ImGuiKey_UpArrow,
                    ImGuiKey_DownArrow,
                    ImGuiKey_PageUp,
                    ImGuiKey_PageDown,
                    ImGuiKey_Home,
                    ImGuiKey_End,
                    ImGuiKey_Insert,
                    ImGuiKey_Delete,
                    ImGuiKey_Backspace,
                    ImGuiKey_Space,
                    ImGuiKey_Enter,
                    ImGuiKey_Escape,
                    ImGuiKey_KeyPadEnter,
                    ImGuiKey_A,
                    ImGuiKey_C,
                    ImGuiKey_V,
                    ImGuiKey_X,
                    ImGuiKey_Y,
                    ImGuiKey_Z,
                    ImGuiKey_COUNT,
                    ImGuiKey_,
                    ImGuiMouseCursor_,
                    ImGuiMouseCursor_None,
                    ImGuiMouseCursor_Arrow,
                    ImGuiMouseCursor_TextInput,
                    ImGuiMouseCursor_ResizeAll,
                    ImGuiMouseCursor_ResizeNS,
                    ImGuiMouseCursor_ResizeEW,
                    ImGuiMouseCursor_ResizeNESW,
                    ImGuiMouseCursor_ResizeNWSE,
                    ImGuiMouseCursor_Hand,
                    ImGuiMouseCursor_NotAllowed,
                    ImGuiMouseCursor_COUNT,};

pub fn check_version() {
    unsafe{
        assert!(igDebugCheckVersionAndDataLayout(cstr!("1.79"),
                                         std::mem::size_of::<ImGuiIO>(),
                                         std::mem::size_of::<ImGuiStyle>(),
                                         std::mem::size_of::<ImVec2>(),
                                         std::mem::size_of::<ImVec4>(),
                                         std::mem::size_of::<ImDrawVert>(),
                                         std::mem::size_of::<ImDrawIdx>()));
    }
}

pub fn create_context() {
    unsafe {
        igCreateContext(std::ptr::null_mut());
    }
}

pub struct IO {
    pub io: *mut ImGuiIO,
}

impl IO {
    pub fn backend_flags(&self) -> &mut ImGuiBackendFlags {
        unsafe {
            &mut (*self.io).BackendFlags
        }
    }

    pub fn keymap(&self, key: ImGuiKey_) -> &mut i32 {
        unsafe {
            &mut (*self.io).KeyMap[key as usize]
        }
    }

    pub fn set_keymap(&self, key: ImGuiKey_, value: i32) {
        unsafe {
            (*self.io).KeyMap[key as usize] = value;
        }
    }

    pub fn set_set_clipboard_text_fn(&self, f: unsafe extern "C" fn(
        user_data: *mut ::std::os::raw::c_void,
        text: *const ::std::os::raw::c_char,
    )) {
        unsafe {
            (*self.io).SetClipboardTextFn = Some(f)
        }
    }

    pub fn set_get_clipboard_text_fn(&self, f: unsafe extern "C" fn(
        user_data: *mut ::std::os::raw::c_void,
    ) -> *const ::std::os::raw::c_char) {
        unsafe {
            (*self.io).GetClipboardTextFn = Some(f)
        }
    }

    pub fn set_clipboard_user_data(&self, data: *mut ()) {
        unsafe {
            (*self.io).ClipboardUserData = data.cast();
        }
    }

    pub fn set_ime_window_handle(&self, handle: *mut ()) {
        unsafe {
            (*self.io).ImeWindowHandle = handle.cast();
        }
    }
}

pub fn get_io() -> IO {
    unsafe {
        let io = igGetIO();
        IO {
            io
        }
    }
}

pub fn style_colors_dark() {
    unsafe {
        igStyleColorsDark(std::ptr::null_mut());
    }
}

pub fn get_cursor_screen_pos() -> ImVec2 {
    unsafe {
        let mut vec = ImVec2 {x:0.0, y:0.0};
        igGetCursorScreenPos(&mut vec);
        vec
    }
}

pub fn get_item_rect_size() -> ImVec2 {
    unsafe {
        let mut vec = ImVec2 {x:0.0, y:0.0};
        igGetItemRectSize(&mut vec);
        vec
    }
}