use crate::imgui;
use std::ffi::{CString, CStr};
use std::os::raw::c_void;
use cimgui::{ImDrawData, ImDrawVert, ImDrawIdx, ImVec4};
use crate::c::{sizeof, nullptr};

static mut GL_VERSION : u32 = 0;

static mut IMGUI_SHADER: u32 = 0;
static mut IMGUI_SHADER_TEXTURE: i32 = 0;
static mut IMGUI_SHADER_PROJ_MTX: i32 = 0;
static mut IMGUI_SHADER_VTX_POS: i32 = 0;
static mut IMGUI_SHADER_VTX_UV: i32 = 0;
static mut IMGUI_SHADER_VTX_COLOR: i32 = 0;

static mut IMGUI_VAO: u32 = 0;
static mut IMGUI_VBO: u32 = 0;
static mut IMGUI_EBO: u32 = 0;
static mut IMGUI_FONT_TEXTURE: u32 = 0;

static mut SCREEN_SHADER: u32 = 0;
static mut SCREEN_SHADER_FRAMEBUFFER_TEXTURE: i32 = 0;
static mut SCREEN_SHADER_QUAD_POS: i32 = 0;
static mut SCREEN_SHADER_QUAD_TEX_COORD: i32 = 0;
static mut SCREEN_SHADER_SCREEN_SIZE: i32 = 0;
static mut SCREEN_SHADER_TIME: i32 = 0;

static mut SCREEN_VAO: u32 = 0;
static mut SCREEN_VBO: u32 = 0;
static mut SCREEN_RBO: u32 = 0;

static mut FRAMEBUFFER: u32 = 0;
static mut FRAMEBUFFER_TEXTUE: u32 = 0;

fn check_gl_errors() {
    tracy_scope!();
    unsafe {
        let error = gl::GetError();
        if error != gl::NO_ERROR {
            panic!("GL Error: {:#x}", error);
        }
    }
}

fn check_shader_compile_error(shader_handle: u32) {
    unsafe {
        let mut success = 0;
        gl::GetShaderiv(shader_handle, gl::COMPILE_STATUS, &mut success);
        if success == 0 {
            const LOG_LENGTH: usize = 1024;
            let mut length = 0;
            let mut log: [i8; LOG_LENGTH] = [0; LOG_LENGTH];
            gl::GetShaderInfoLog(shader_handle, LOG_LENGTH as i32, &mut length, log.as_mut_ptr());
            let log = CStr::from_ptr(log.as_ptr()).to_str().unwrap();
            panic!("GL Shader compile error:\n{}", log);
        }
    }
}

fn check_shader_link_error(shader_handle: u32) {
    unsafe {
        let mut success = 0;
        gl::GetProgramiv(shader_handle, gl::LINK_STATUS, &mut success);
        if success == 0 {
            const LOG_LENGTH: usize = 1024;
            let mut length = 0;
            let mut log: [i8; LOG_LENGTH] = [0; LOG_LENGTH];
            gl::GetProgramInfoLog(shader_handle, LOG_LENGTH as i32, &mut length, log.as_mut_ptr());
            let log = CStr::from_ptr(log.as_ptr()).to_str().unwrap();
            panic!("GL Shader link error:\n{}", log);
        }
    }
}

fn create_fonts_texture() {
    tracy_scope!();
    check_gl_errors();
    unsafe {
        let io = imgui::get_io();
        let mut pixels= std::ptr::null_mut();
        let mut width = 0;
        let mut height= 0;
        cimgui::ImFontAtlas_GetTexDataAsRGBA32((*io.io).Fonts, &mut pixels, &mut width, &mut height, std::ptr::null_mut());

        let mut last_texture = 0;
        gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut last_texture);
        check_gl_errors();
        gl::GenTextures(1, &mut IMGUI_FONT_TEXTURE);
        check_gl_errors();
        gl::BindTexture(gl::TEXTURE_2D, IMGUI_FONT_TEXTURE);
        check_gl_errors();
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
        check_gl_errors();
        gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        check_gl_errors();

        gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGBA as i32, width, height, 0, gl::RGBA, gl::UNSIGNED_BYTE, pixels as *const c_void);
        check_gl_errors();

        (*(*io.io).Fonts).TexID = IMGUI_FONT_TEXTURE as *mut c_void;

        gl::BindTexture(gl::TEXTURE_2D, last_texture as u32);
        check_gl_errors();
    }
    check_gl_errors();
}

pub fn imgui_init_gl3() {
    tracy_scope!();
    let io = imgui::get_io();

    unsafe {
        (*io.io).BackendRendererName = cstr!("imgui_impl_opengl3");
        (*io.io).BackendFlags |= cimgui::ImGuiBackendFlags_RendererHasVtxOffset as i32;
        let mut major = 0;
        let mut minor = 0;
        gl::GetIntegerv(gl::MAJOR_VERSION, &mut major);
        gl::GetIntegerv(gl::MINOR_VERSION, &mut minor);
        GL_VERSION = (major * 100 + minor * 10) as u32;
        println!("GL Version: {}", GL_VERSION);
    }

    unsafe {
        let mut last_texture = 0;
        let mut last_array_buffer = 0;
        gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut last_texture);
        gl::GetIntegerv(gl::ARRAY_BUFFER_BINDING, &mut last_array_buffer);
        check_gl_errors();

        let fb_width = 1600;
        let fb_height = 900;
        {
            {
                let vertex_shader = std::include_str!("shaders/imgui_vertex.glsl");
                let vertex_shader = CString::new(vertex_shader).unwrap();
                let vert_handle = gl::CreateShader(gl::VERTEX_SHADER);
                gl::ShaderSource(vert_handle, 1, &vertex_shader.as_ptr(), std::ptr::null_mut());
                gl::CompileShader(vert_handle);
                check_shader_compile_error(vert_handle);
                check_gl_errors();

                let fragment_shader = std::include_str!("shaders/imgui_fragment.glsl");
                let fragment_shader = CString::new(fragment_shader).unwrap();
                let frag_handle = gl::CreateShader(gl::FRAGMENT_SHADER);
                gl::ShaderSource(frag_handle, 1, &fragment_shader.as_ptr(), std::ptr::null_mut());
                gl::CompileShader(frag_handle);
                check_shader_compile_error(frag_handle);
                check_gl_errors();

                IMGUI_SHADER = gl::CreateProgram();
                gl::AttachShader(IMGUI_SHADER, vert_handle);
                gl::AttachShader(IMGUI_SHADER, frag_handle);
                gl::LinkProgram(IMGUI_SHADER);
                check_gl_errors();

                IMGUI_SHADER_TEXTURE = gl::GetUniformLocation(IMGUI_SHADER, cstr!("Texture"));
                check_gl_errors();
                IMGUI_SHADER_PROJ_MTX = gl::GetUniformLocation(IMGUI_SHADER, cstr!("ProjMtx"));
                check_gl_errors();
                IMGUI_SHADER_VTX_POS = gl::GetAttribLocation(IMGUI_SHADER, cstr!("Position"));
                check_gl_errors();
                IMGUI_SHADER_VTX_UV = gl::GetAttribLocation(IMGUI_SHADER, cstr!("UV"));
                check_gl_errors();
                IMGUI_SHADER_VTX_COLOR = gl::GetAttribLocation(IMGUI_SHADER, cstr!("Color"));
                check_gl_errors();

                gl::UseProgram(IMGUI_SHADER);
                check_gl_errors();
                gl::Uniform1i(IMGUI_SHADER_TEXTURE, 0);
                check_gl_errors();
            }

            create_fonts_texture();

            {
                gl::GenVertexArrays(1, &mut IMGUI_VAO);
                gl::GenBuffers(1, &mut IMGUI_VBO);
                check_gl_errors();
                gl::GenBuffers(1, &mut IMGUI_EBO);
                check_gl_errors();

                gl::BindVertexArray(IMGUI_VAO);
                gl::BindBuffer(gl::ARRAY_BUFFER, IMGUI_VBO);
                check_gl_errors();
                gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, IMGUI_EBO);
                check_gl_errors();
                gl::EnableVertexAttribArray(IMGUI_SHADER_VTX_POS as u32);
                check_gl_errors();
                if IMGUI_SHADER_VTX_UV > 0 {
                    gl::EnableVertexAttribArray(IMGUI_SHADER_VTX_UV as u32);
                    check_gl_errors();
                }
                if IMGUI_SHADER_VTX_COLOR > 0 {
                    gl::EnableVertexAttribArray(IMGUI_SHADER_VTX_COLOR as u32);
                    check_gl_errors();
                }
                gl::VertexAttribPointer(IMGUI_SHADER_VTX_POS as u32, 2, gl::FLOAT, gl::FALSE, std::mem::size_of::<ImDrawVert>() as i32, offsetof!(ImDrawVert, pos) as *const c_void);
                check_gl_errors();
                if IMGUI_SHADER_VTX_UV > 0 {
                    gl::VertexAttribPointer(IMGUI_SHADER_VTX_UV as u32, 2, gl::FLOAT, gl::FALSE, std::mem::size_of::<ImDrawVert>() as i32, offsetof!(ImDrawVert, uv) as *const c_void);
                    check_gl_errors();
                }
                if IMGUI_SHADER_VTX_COLOR > 0 {
                    gl::VertexAttribPointer(IMGUI_SHADER_VTX_COLOR as u32, 4, gl::UNSIGNED_BYTE, gl::TRUE, std::mem::size_of::<ImDrawVert>() as i32, offsetof!(ImDrawVert, col) as *const c_void);
                    check_gl_errors();
                }

                gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
                gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                gl::BindVertexArray(0);
            }

            {
                let vertex_shader = std::include_str!("shaders/screen_vertex.glsl");
                let vertex_shader = CString::new(vertex_shader).unwrap();
                let vert_handle = gl::CreateShader(gl::VERTEX_SHADER);
                gl::ShaderSource(vert_handle, 1, &vertex_shader.as_ptr(), std::ptr::null_mut());
                gl::CompileShader(vert_handle);
                check_shader_compile_error(vert_handle);
                check_gl_errors();

                let fragment_shader = std::include_str!("shaders/screen_fragment.glsl");
                let fragment_shader = CString::new(fragment_shader).unwrap();
                let frag_handle = gl::CreateShader(gl::FRAGMENT_SHADER);
                gl::ShaderSource(frag_handle, 1, &fragment_shader.as_ptr(), std::ptr::null_mut());
                gl::CompileShader(frag_handle);
                check_shader_compile_error(frag_handle);
                check_gl_errors();

                SCREEN_SHADER = gl::CreateProgram();
                gl::AttachShader(SCREEN_SHADER, vert_handle);
                gl::AttachShader(SCREEN_SHADER, frag_handle);
                gl::LinkProgram(SCREEN_SHADER);
                let mut success = 0;
                gl::GetProgramiv(SCREEN_SHADER, gl::LINK_STATUS, &mut success);
                check_shader_link_error(SCREEN_SHADER);
                check_gl_errors();

                SCREEN_SHADER_QUAD_POS = gl::GetAttribLocation(SCREEN_SHADER, cstr!("aPos"));
                check_gl_errors();
                SCREEN_SHADER_QUAD_TEX_COORD = gl::GetAttribLocation(SCREEN_SHADER, cstr!("aTexCoords"));
                check_gl_errors();
                SCREEN_SHADER_FRAMEBUFFER_TEXTURE = gl::GetUniformLocation(SCREEN_SHADER, cstr!("screenTexture"));
                check_gl_errors();
                SCREEN_SHADER_SCREEN_SIZE = gl::GetUniformLocation(SCREEN_SHADER, cstr!("screenSize"));
                check_gl_errors();
                SCREEN_SHADER_TIME = gl::GetUniformLocation(SCREEN_SHADER, cstr!("time"));
                check_gl_errors();

                gl::UseProgram(SCREEN_SHADER);
                check_gl_errors();

                gl::Uniform1i(SCREEN_SHADER_FRAMEBUFFER_TEXTURE, 0);
                check_gl_errors();

                gl::Uniform2f(SCREEN_SHADER_SCREEN_SIZE, fb_width as f32, fb_height as f32);
                check_gl_errors();
            }

            {
                gl::GenVertexArrays(1, &mut SCREEN_VAO);
                gl::GenBuffers(1, &mut SCREEN_VBO);

                // Bind vertex/index buffers and setup attributes for ImDrawVert
                gl::BindVertexArray(SCREEN_VAO);
                gl::BindBuffer(gl::ARRAY_BUFFER, SCREEN_VBO);
                check_gl_errors();
                {
                    let quad_vertices: [f32; 24] = [ // vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
                        // positions   // texCoords
                        -1.0, 1.0, 0.0, 1.0,
                        -1.0, -1.0, 0.0, 0.0,
                        1.0, -1.0, 1.0, 0.0,
                        -1.0, 1.0, 0.0, 1.0,
                        1.0, -1.0, 1.0, 0.0,
                        1.0, 1.0, 1.0, 1.0
                    ];

                    gl::BufferData(gl::ARRAY_BUFFER, (quad_vertices.len() * sizeof::<f32>()) as isize, quad_vertices.as_ptr() as *const c_void, gl::STATIC_DRAW);
                    check_gl_errors();
                }

                check_gl_errors();
                gl::VertexAttribPointer(SCREEN_SHADER_QUAD_POS as u32, 2, gl::FLOAT, gl::FALSE, (4 * sizeof::<f32>()) as i32, 0 as *const c_void);
                check_gl_errors();
                gl::EnableVertexAttribArray(SCREEN_SHADER_QUAD_POS as u32);
                check_gl_errors();
                gl::VertexAttribPointer(SCREEN_SHADER_QUAD_TEX_COORD as u32, 2, gl::FLOAT, gl::FALSE, (4 * sizeof::<f32>()) as i32, (2 * sizeof::<f32>()) as *const c_void);
                check_gl_errors();
                gl::EnableVertexAttribArray(SCREEN_SHADER_QUAD_TEX_COORD as u32);
                check_gl_errors();

                gl::BindBuffer(gl::ARRAY_BUFFER, 0);
                gl::BindVertexArray(0);
            }

            {   // framebuffer configuration
                gl::GenFramebuffers(1, &mut FRAMEBUFFER);
                gl::BindFramebuffer(gl::FRAMEBUFFER, FRAMEBUFFER);
                check_gl_errors();

                gl::GenTextures(1, &mut FRAMEBUFFER_TEXTUE);
                gl::BindTexture(gl::TEXTURE_2D, FRAMEBUFFER_TEXTUE);
                gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, fb_width, fb_height, 0, gl::RGB, gl::UNSIGNED_BYTE, nullptr());
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
                gl::FramebufferTexture2D(gl::FRAMEBUFFER, gl::COLOR_ATTACHMENT0, gl::TEXTURE_2D, FRAMEBUFFER_TEXTUE, 0);
                check_gl_errors();

                gl::GenRenderbuffers(1, &mut SCREEN_RBO);
                gl::BindRenderbuffer(gl::RENDERBUFFER, SCREEN_RBO);
                gl::RenderbufferStorage(gl::RENDERBUFFER, gl::DEPTH24_STENCIL8, fb_width, fb_height); // use a single renderbuffer object for both a depth AND stencil buffer.
                gl::FramebufferRenderbuffer(gl::FRAMEBUFFER, gl::DEPTH_STENCIL_ATTACHMENT, gl::RENDERBUFFER, SCREEN_RBO); // now actually attach it
                check_gl_errors();

                // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
                let framebuffer_status = gl::CheckFramebufferStatus(gl::FRAMEBUFFER);
                if framebuffer_status != gl::FRAMEBUFFER_COMPLETE {
                    if framebuffer_status == gl::FRAMEBUFFER_UNDEFINED {
                        panic!("GL Framebuffer is not complete, the specified framebuffer is the default read or draw framebuffer, but the default framebuffer does not exist.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_ATTACHMENT {
                        panic!("GL Framebuffer is not complete, one of the framebuffer attachment points is framebuffer incomplete.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT {
                        panic!("GL Framebuffer is not complete, the framebuffer does not have at least one image attached to it.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER {
                        panic!("GL Framebuffer is not complete, the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAW_BUFFERi.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_READ_BUFFER {
                        panic!("GL Framebuffer is not complete, GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_UNSUPPORTED {
                        panic!("GL Framebuffer is not complete, the combination of internal formats of the attached images violates an implementation-dependent set of restrictions.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_MULTISAMPLE {
                        panic!("GL Framebuffer is not complete, the value of GL_RENDERBUFFER_SAMPLES is not the same for all attached renderbuffers; if the value of GL_TEXTURE_SAMPLES is the not same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_RENDERBUFFER_SAMPLES does not match the value of GL_TEXTURE_SAMPLES; or the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures.");
                    }
                    if framebuffer_status == gl::FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS {
                        panic!("GL Framebuffer is not complete, a framebuffer attachment is layered, or a populated attachment is not layered, or all populated color attachments are not from textures of the same target.");
                    }
                    panic!("GL Framebuffer is not complete");
                }
            }
            gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
        }
        gl::BindTexture(gl::TEXTURE_2D, last_texture as u32);
        gl::BindBuffer(gl::ARRAY_BUFFER, last_array_buffer as u32);
    }
    check_gl_errors();
}

pub fn window_resize(width: i32, height: i32) {
    unsafe {
        println!("Window resized: {}x{}", width, height);

        gl::BindTexture(gl::TEXTURE_2D, FRAMEBUFFER_TEXTUE);
        check_gl_errors();
        gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB as i32, width, height, 0, gl::RGB, gl::UNSIGNED_BYTE, nullptr());
        check_gl_errors();

        gl::BindRenderbuffer(gl::RENDERBUFFER, SCREEN_RBO);
        gl::RenderbufferStorage(gl::RENDERBUFFER, gl::DEPTH24_STENCIL8, width, height); // use a single renderbuffer object for both a depth AND stencil buffer.
        check_gl_errors();

        gl::UseProgram(SCREEN_SHADER);
        check_gl_errors();
        gl::Uniform2f(SCREEN_SHADER_SCREEN_SIZE, width as f32, height as f32);
        check_gl_errors();
    }
}

fn setup_imgui_render_state(draw_data: *mut ImDrawData, fb_width: i32, fb_height: i32) {
    tracy_scope!();
    unsafe{
        // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, polygon fill
        gl::Enable(gl::BLEND);
        gl::BlendEquation(gl::FUNC_ADD);
        gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
        gl::Disable(gl::CULL_FACE);
        gl::Disable(gl::DEPTH_TEST);
        gl::Enable(gl::SCISSOR_TEST);
        check_gl_errors();

        // Support for GL 4.5 rarely used glClipControl(GL_UPPER_LEFT)
        let mut clip_origin_lower_left = true;
        let mut current_clip_origin = 0;
        gl::GetIntegerv(gl::CLIP_ORIGIN, &mut current_clip_origin);
        check_gl_errors();
        if current_clip_origin == gl::UPPER_LEFT as i32 {
            clip_origin_lower_left = false;
        }

        // Setup viewport, orthographic projection matrix
        // Our visible imgui space lies from (*draw_data).DisplayPos (top left) to (*draw_data).DisplayPos+(*draw_data).DisplaySize (bottom right). DisplayPos is (0,0) for single viewport apps.
        gl::Viewport(0, 0, fb_width, fb_height);
        check_gl_errors();
        let l = (*draw_data).DisplayPos.x;
        let r = (*draw_data).DisplayPos.x + (*draw_data).DisplaySize.x;
        let mut t = (*draw_data).DisplayPos.y;
        let mut b = (*draw_data).DisplayPos.y + (*draw_data).DisplaySize.y;
        if !clip_origin_lower_left
        {
            let tmp = t;
            t = b;
            b = tmp;
        }
        // Swap top and bottom if origin is upper left
        let ortho_projection: [[f32;4];4] =
            [
                [ 2.0/(r - l),   0.0,          0.0,   0.0 ],
                [ 0.0,         2.0/(t - b),    0.0,   0.0 ],
                [ 0.0,         0.0,         -1.0,   0.0 ],
                [ (r + l)/(l - r), (t + b)/(b - t),  0.0,   1.0 ],
            ];
        gl::UseProgram(IMGUI_SHADER);
        check_gl_errors();

        gl::UniformMatrix4fv(IMGUI_SHADER_PROJ_MTX, 1, gl::FALSE, &ortho_projection[0][0]);
        check_gl_errors();

        gl::BindVertexArray(IMGUI_VAO);
        check_gl_errors();
        gl::BindBuffer(gl::ARRAY_BUFFER, IMGUI_VBO);
        check_gl_errors();
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, IMGUI_EBO);
        check_gl_errors();
    }
}

fn render_imgui(draw_data: *mut ImDrawData, fb_width: i32, fb_height: i32) {
    unsafe {
        // Setup desired GL state
        setup_imgui_render_state(draw_data, fb_width, fb_height);

        // Will project scissor/clipping rectangles into framebuffer space
        let clip_off = (*draw_data).DisplayPos;         // (0,0) unless using multi-viewports
        let clip_scale = (*draw_data).FramebufferScale; // (1,1) unless using retina display which are often (2,2)

        // Render command lists
        for n in 0..(*draw_data).CmdListsCount {
            let cmd_list = (*draw_data).CmdLists.offset(n as isize);

            // Upload vertex/index buffers
            gl::BufferData(gl::ARRAY_BUFFER, ((*(*cmd_list)).VtxBuffer.Size * std::mem::size_of::<ImDrawVert>() as i32) as isize, (*(*cmd_list)).VtxBuffer.Data as *const c_void, gl::STREAM_DRAW);
            check_gl_errors();
            gl::BufferData(gl::ELEMENT_ARRAY_BUFFER, ((*(*cmd_list)).IdxBuffer.Size * std::mem::size_of::<ImDrawIdx>() as i32) as isize, (*(*cmd_list)).IdxBuffer.Data as *const c_void, gl::STREAM_DRAW);
            check_gl_errors();

            for cmd_i in 0..(*(*cmd_list)).CmdBuffer.Size {
                let pcmd = &(*(*cmd_list)).CmdBuffer.Data.offset(cmd_i as isize);

                if (*(*pcmd)).UserCallback.is_some()
                {
                    // User callback, registered via ImDrawList::AddCallback()
                    // (ImDrawCallback_ResetRenderState is a special callback value used by the user to request the renderer to reset render state.)

                    if (*(*pcmd)).UserCallback.unwrap() as *const c_void == -1isize as *const c_void
                    {
                        setup_imgui_render_state(draw_data, fb_width, fb_height);
                    } else {
                        (*(*pcmd)).UserCallback.unwrap()(*cmd_list, *pcmd);
                    }
                } else {
                    // Project scissor/clipping rectangles into framebuffer space
                    let clip_rect = ImVec4 {
                        x: ((*(*pcmd)).ClipRect.x - clip_off.x) * clip_scale.x,
                        y: ((*(*pcmd)).ClipRect.y - clip_off.y) * clip_scale.y,
                        z: ((*(*pcmd)).ClipRect.z - clip_off.x) * clip_scale.x,
                        w: ((*(*pcmd)).ClipRect.w - clip_off.y) * clip_scale.y
                    };

                    if clip_rect.x < fb_width as f32 && clip_rect.y < fb_height as f32 && clip_rect.z >= 0.0 && clip_rect.w >= 0.0
                    {
                        // Apply scissor/clipping rectangle
                        gl::Scissor(clip_rect.x as i32, (fb_height as f32 - clip_rect.w) as i32, (clip_rect.z - clip_rect.x) as i32, (clip_rect.w - clip_rect.y) as i32);

                        // Bind texture, Draw
                        gl::BindTexture(gl::TEXTURE_2D, (*(*pcmd)).TextureId as u32);
                        gl::DrawElementsBaseVertex(gl::TRIANGLES, (*(*pcmd)).ElemCount as i32, if sizeof::<ImDrawIdx>() == 2 { gl::UNSIGNED_SHORT } else { gl::UNSIGNED_INT }, ((*(*pcmd)).IdxOffset as usize * sizeof::<ImDrawIdx>()) as *const c_void, (*(*pcmd)).VtxOffset as i32);
                        //gl::DrawElementsBaseVertex(gl::TRIANGLES, (*(*pcmd)).ElemCount as i32, if std::mem::size_of::<ImDrawIdx>() == 2 {gl::UNSIGNED_SHORT} else {gl::UNSIGNED_INT}, ((*(*pcmd)).IdxOffset * std::mem::size_of::<ImDrawIdx>() as u32) as *const c_void, (*(*pcmd)).VtxOffset as i32);
                    }
                }
                check_gl_errors();
            }
        }

        gl::BindBuffer(gl::ARRAY_BUFFER, 0);
        check_gl_errors();
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        check_gl_errors();
    }
}

pub fn render_screen(time: f32) {
    tracy_scope!();
    unsafe {
        let draw_data = cimgui::igGetDrawData();
        // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
        let fb_width = ((*draw_data).DisplaySize.x * (*draw_data).FramebufferScale.x) as i32;
        let fb_height = ((*draw_data).DisplaySize.y * (*draw_data).FramebufferScale.y) as i32;
        if fb_width <= 0 || fb_height <= 0
        {
            return;
        }

        // Backup GL state
        let mut last_active_texture = 0;
        gl::GetIntegerv(gl::ACTIVE_TEXTURE, &mut last_active_texture);
        check_gl_errors();
        gl::ActiveTexture(gl::TEXTURE0);
        let mut last_program = 0;
        gl::GetIntegerv(gl::CURRENT_PROGRAM, &mut last_program);
        let mut last_texture = 0;
        gl::GetIntegerv(gl::TEXTURE_BINDING_2D, &mut last_texture);

        let mut last_array_buffer = 0;
        gl::GetIntegerv(gl::ARRAY_BUFFER_BINDING, &mut last_array_buffer);

        let mut last_viewport: [i32; 4] = [0; 4];
        gl::GetIntegerv(gl::VIEWPORT, last_viewport.as_mut_ptr());
        let mut last_scissor_box: [i32; 4] = [0; 4];
        gl::GetIntegerv(gl::SCISSOR_BOX, last_scissor_box.as_mut_ptr());
        let mut last_blend_src_rgb = 0;
        gl::GetIntegerv(gl::BLEND_SRC_RGB, &mut last_blend_src_rgb);
        let mut last_blend_dst_rgb = 0;
        gl::GetIntegerv(gl::BLEND_DST_RGB, &mut last_blend_dst_rgb);
        let mut last_blend_src_alpha = 0;
        gl::GetIntegerv(gl::BLEND_SRC_ALPHA, &mut last_blend_src_alpha);
        let mut last_blend_dst_alpha = 0;
        gl::GetIntegerv(gl::BLEND_DST_ALPHA, &mut last_blend_dst_alpha);
        let mut last_blend_equation_rgb = 0;
        gl::GetIntegerv(gl::BLEND_EQUATION_RGB, &mut last_blend_equation_rgb);
        let mut last_blend_equation_alpha = 0;
        gl::GetIntegerv(gl::BLEND_EQUATION_ALPHA, &mut last_blend_equation_alpha);
        let last_enable_blend = gl::IsEnabled(gl::BLEND) != 0;
        let last_enable_cull_face = gl::IsEnabled(gl::CULL_FACE) != 0;
        let last_enable_depth_test = gl::IsEnabled(gl::DEPTH_TEST) != 0;
        let last_enable_scissor_test = gl::IsEnabled(gl::SCISSOR_TEST) != 0;
        check_gl_errors();

        {
            gl::BindFramebuffer(gl::FRAMEBUFFER, FRAMEBUFFER);
            check_gl_errors();
            gl::ClearColor(0.15, 0.15, 0.15, 1.0);
            check_gl_errors();
            gl::Clear(gl::COLOR_BUFFER_BIT);
            check_gl_errors();

            render_imgui(draw_data, fb_width, fb_height);

            {
                gl::Disable(gl::SCISSOR_TEST);
                // now bind back to default framebuffer and draw a quad plane with the attached framebuffer color texture
                gl::BindFramebuffer(gl::FRAMEBUFFER, 0);
                check_gl_errors();

                gl::UseProgram(SCREEN_SHADER);
                check_gl_errors();
                gl::Uniform1f(SCREEN_SHADER_TIME, time);
                check_gl_errors();

                gl::BindVertexArray(SCREEN_VAO);
                check_gl_errors();

                gl::BindTexture(gl::TEXTURE_2D, FRAMEBUFFER_TEXTUE);    // use the color attachment texture as the texture of the quad plane
                check_gl_errors();
                gl::DrawArrays(gl::TRIANGLES, 0, 6);
                check_gl_errors();

                gl::BindVertexArray(0);
            }
        }

        // Restore modified GL state
        gl::UseProgram(last_program as u32);
        gl::BindTexture(gl::TEXTURE_2D, last_texture as u32);
        gl::ActiveTexture(last_active_texture as u32);
        gl::BindBuffer(gl::ARRAY_BUFFER, last_array_buffer as u32);
        gl::BlendEquationSeparate(last_blend_equation_rgb as u32, last_blend_equation_alpha as u32);
        gl::BlendFuncSeparate(last_blend_src_rgb as u32, last_blend_dst_rgb as u32, last_blend_src_alpha as u32, last_blend_dst_alpha as u32);
        if last_enable_blend
        {
            gl::Enable(gl::BLEND);
        }
        else {
            gl::Disable(gl::BLEND);
        }

        if last_enable_cull_face
        {
            gl::Enable(gl::CULL_FACE);
        }
        else {
            gl::Disable(gl::CULL_FACE);
        }

        if last_enable_depth_test
        {
            gl::Enable(gl::DEPTH_TEST);
        }
        else {
            gl::Disable(gl::DEPTH_TEST);
        }

        if last_enable_scissor_test
        {
            gl::Enable(gl::SCISSOR_TEST);
        }
        else {
            gl::Disable(gl::SCISSOR_TEST);
        }

        gl::Viewport(last_viewport[0], last_viewport[1], last_viewport[2], last_viewport[3]);
        gl::Scissor(last_scissor_box[0], last_scissor_box[1], last_scissor_box[2], last_scissor_box[3]);

    }
    check_gl_errors();
}