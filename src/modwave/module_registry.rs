use std::collections::HashMap;
use crate::modwave::module::Module;

type ModuleConstructor = Box<dyn Fn() -> Box<dyn Module>>;

//static mut REGISTRY: Option<HashMap<&'static str, ModuleConstructor>> = None;

pub static mut REGISTRY: Option<Registry> = None;

pub struct Registry {
    modules: HashMap<&'static str, ModuleConstructor>
}

impl Registry {
    pub fn new() -> Registry {
        Registry{
            modules: HashMap::new()
        }
    }

    pub fn register_module(&mut self, module_name: &'static str, constructor: ModuleConstructor) {
        self.modules.insert(module_name, constructor);
    }

    pub fn new_module(&self, module_name: &'static str) -> Box<dyn Module> {
        self.modules.get(module_name).unwrap()()
    }

    pub fn get_registered_modules(&self) -> Vec<&'static str> {
        let mut list: Vec<&'static str> = self.modules.keys().cloned().collect();
        list.sort_unstable();
        list
    }
}
/*
pub fn register_module(module_name: &'static str, constructor: ModuleConstructor) {
    unsafe {
        REGISTRY.unwrap().insert(module_name, constructor);
    };
}

pub fn new_module(module_name: &'static str) -> Box<dyn Module> {
    unsafe {
        REGISTRY.unwrap().get(module_name).unwrap()()
    }
}

pub fn get_registered_modules() -> Vec<&'static str> {
    let mut modules: Vec<&'static str> = unsafe {
        REGISTRY.unwrap().keys().cloned().collect()
    };
    modules.sort_unstable();
    modules
}

 */