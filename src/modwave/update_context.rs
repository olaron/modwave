use crate::modwave::BufferStore;

pub struct UpdateContext {
    pub store: BufferStore,
    pub length: usize,
    pub sample_rate: usize,
}

impl UpdateContext {
    pub fn new(buffer_length: usize, sampling_rate: usize) -> UpdateContext {
        UpdateContext {
            store: BufferStore::new(),
            length: buffer_length,
            sample_rate: sampling_rate,
        }
    }
}