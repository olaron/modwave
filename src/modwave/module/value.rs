use crate::modwave::{Sample, Registry, StereoBuffer, UpdateContext};
use crate::modwave::module::{Module, ModuleIOName, Voices};

pub struct Value {
    min: Sample,
    max: Sample,
    value: Sample,
}

impl Value {
    pub fn new(value: Sample) -> Value {
        Value {
            min: -1.0,
            max: 1.0,
            value
        }
    }

    fn name() -> &'static str {
        "Value"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Value::name(), Box::new(||{Box::from(Value::new(0.0))}));
    }
}

impl Module for Value {
    fn name(&self) -> &'static str {
        Value::name()
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["output"]
    }

    fn update_voices(&mut self,
                     _: &mut UpdateContext,
                     _: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut output_voices = Voices::new(1);
        output_voices.insert(0, vec![StereoBuffer::constant(self.value)]);
        output_voices
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            cimgui::igInputDouble(cstr!("Min"), &mut self.min, 0.0, 0.0,cstr!("%.3f"),0);
            let mut val = self.value as f32;
            cimgui::igDragFloat(cstr!("Val"), &mut val, ((self.max - self.min) / 200.0) as f32, self.min as f32, self.max as f32, cstr!("%.3f"), 0);
            self.value = val as Sample;
            cimgui::igInputDouble(cstr!("Max"), &mut self.max, 0.0, 0.0,cstr!("%.3f"),0);
        }
    }
}