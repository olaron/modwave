use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, StereoSample, UpdateContext, StereoBuffer, Sample};
use std::ffi::CString;
use cimgui::ImVec2;

pub struct Operator{
    selected_operator: &'static str,
}

impl Operator {
    pub fn new(selected_operator: &'static str) -> Operator {
        Operator {
            selected_operator,
        }
    }

    pub fn name() -> &'static str {
        "Operator"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Operator::name(), Box::new(||{Box::from(Operator::new("+"))}));
    }
}

impl Module for Operator {
    fn name(&self) -> &'static str {
        Operator::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["a", "b"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["output"]
    }


    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     inputs_buffers: &Vec<StereoBuffer>,
                     _voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let n_outputs = self.outputs().len();
        let mut output_buffers = Vec::with_capacity(n_outputs);
        for _ in 0..n_outputs {
            output_buffers.push(context.store.allocate_stereo(context.length));
        }
        match self.selected_operator {
            "+" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(inputs[0].left+inputs[1].left,inputs[0].right+inputs[1].right)];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "-" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(inputs[0].left-inputs[1].left,inputs[0].right-inputs[1].right)];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "*" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(inputs[0].left*inputs[1].left,inputs[0].right*inputs[1].right)];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "/" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(inputs[0].left/inputs[1].left,inputs[0].right/inputs[1].right)];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "%" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(inputs[0].left%inputs[1].left,inputs[0].right%inputs[1].right)];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "and" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        Sample::from_bits(inputs[0].left.to_bits() & inputs[1].left.to_bits()),
                        Sample::from_bits(inputs[0].right.to_bits() & inputs[1].right.to_bits()))];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "or" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        Sample::from_bits(inputs[0].left.to_bits() | inputs[1].left.to_bits()),
                        Sample::from_bits(inputs[0].right.to_bits() | inputs[1].right.to_bits()))];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "xor" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        Sample::from_bits(inputs[0].left.to_bits() ^ inputs[1].left.to_bits()),
                        Sample::from_bits(inputs[0].right.to_bits() ^ inputs[1].right.to_bits()))];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            ">" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        if inputs[0].left > inputs[1].left {1.0} else {0.0},
                        if inputs[0].right > inputs[1].right {1.0} else {0.0})];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            ">=" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        if inputs[0].left >= inputs[1].left {1.0} else {0.0},
                        if inputs[0].right >= inputs[1].right {1.0} else {0.0})];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            "=" => {
                for i in 0..context.length {
                    tracy_scope_named!("sample");
                    let mut inputs = Vec::with_capacity(inputs_buffers.len());
                    for input_buffer in inputs_buffers {
                        inputs.push(input_buffer.at(i));
                    }
                    let outputs = vec![StereoSample::new(
                        if inputs[0].left == inputs[1].left {1.0} else {0.0},
                        if inputs[0].right == inputs[1].right {1.0} else {0.0})];
                    for o in 0..outputs.len() {
                        output_buffers[o].set(i,outputs[o]);
                    }
                }
            },
            _ => {
                unreachable!();
            }
        }
        output_buffers
    }

    fn draw_gui(&mut self) {
        unsafe {
            let operator = CString::new(self.selected_operator).unwrap();
            cimgui::igPushItemWidth(50.0);
            if cimgui::igBeginCombo(cstr!(""), operator.as_ptr(),0) {
                if cimgui::igSelectableBool(cstr!("+"), self.selected_operator == "+", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "+";
                }
                if cimgui::igSelectableBool(cstr!("-"), self.selected_operator == "-", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "-";
                }
                if cimgui::igSelectableBool(cstr!("*"), self.selected_operator == "*", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "*";
                }
                if cimgui::igSelectableBool(cstr!("/"), self.selected_operator == "/", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "/";
                }
                if cimgui::igSelectableBool(cstr!("%"), self.selected_operator == "%", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "%";
                }
                if cimgui::igSelectableBool(cstr!("and"), self.selected_operator == "and", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "and";
                }
                if cimgui::igSelectableBool(cstr!("or"), self.selected_operator == "or", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "or";
                }
                if cimgui::igSelectableBool(cstr!("xor"), self.selected_operator == "xor", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "xor";
                }
                if cimgui::igSelectableBool(cstr!(">"), self.selected_operator == ">", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = ">";
                }
                if cimgui::igSelectableBool(cstr!(">="), self.selected_operator == ">=", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = ">=";
                }
                if cimgui::igSelectableBool(cstr!("="), self.selected_operator == "=", 0, ImVec2::new(0.0, 0.0)) {
                    self.selected_operator = "=";
                }
                cimgui::igEndCombo();
            }
            cimgui::igPopItemWidth();
        }
    }
}