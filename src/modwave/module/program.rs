use std::collections::{HashMap};
use super::{Module};
use crate::modwave::{REGISTRY, StereoBuffer, UpdateContext, Registry};
use cimgui::{ImVec2, ImDrawListSplitter, igGetStyle};
use crate::c::{sizeof, nullptr};
use std::ffi::CString;
use std::os::raw::c_void;
use crate::imgui;
use crate::modwave::module::{ModuleOutputName, ModuleIOName, Voices, VoiceID};
use std::str;
use std::ops::{DerefMut};

pub type ModuleID = u32;

static mut LAST_VOICE_ID: VoiceID = 0;
pub fn new_id() -> VoiceID {
    unsafe {
        LAST_VOICE_ID += 1;
        return LAST_VOICE_ID;
    }
}

pub type UUID = u32;
static mut LAST_UUID: UUID = 0;
pub fn new_uuid() -> UUID {
    unsafe {
        LAST_UUID += 1;
        return LAST_UUID;
    }
}

const NODE_SLOT_RADIUS: f32 = 4.0;
const NODE_WINDOW_PADDING: ImVec2 = ImVec2{x:8.0,y:8.0};

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct InputConnector {
    node: ModuleID,
    slot: usize,
}

impl InputConnector {
    fn new(node: ModuleID, slot: usize) -> InputConnector {
        InputConnector {
            node,
            slot,
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct OutputConnector {
    node: ModuleID,
    slot: usize,
}

impl OutputConnector {
    fn new(node: ModuleID, slot: usize) -> OutputConnector {
        OutputConnector {
            node,
            slot,
        }
    }
}


#[derive(Copy, Clone)]
enum ConnectorKind {
    Input,
    Output,
}

#[derive(Clone)]
struct DraggedConnector {
    program_uuid: UUID,
    kind: ConnectorKind,
    node: ModuleID,
    slot: usize,
}

impl DraggedConnector {
    fn input(program_uuid: UUID, node: ModuleID, slot: usize) -> DraggedConnector {
        DraggedConnector {
            program_uuid,
            kind: ConnectorKind::Input,
            node,
            slot,
        }
    }

    fn output(program_uuid: UUID, node: ModuleID, slot: usize) -> DraggedConnector {
        DraggedConnector {
            program_uuid,
            kind: ConnectorKind::Output,
            node,
            slot,
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct Connection {
    source: OutputConnector,
    destination: InputConnector,
}

impl Connection {
    pub fn new(source: OutputConnector, destination: InputConnector,) -> Connection {
        Connection {
            source,
            destination,
        }
    }

    pub fn new2(source_module: ModuleID, source_slot: usize, destination_module: ModuleID, destination_slot: usize) -> Connection {
        Connection {
            source: OutputConnector::new(source_module, source_slot),
            destination: InputConnector::new(destination_module, destination_slot),
        }
    }
}

struct ModuleUI {
    pos: ImVec2,
    size: ImVec2,
    dragged: bool
}

const MODULE_LINE_HEIGHT: f32 = 19.0;

impl ModuleUI {
    fn new(pos: ImVec2) -> ModuleUI {
        ModuleUI {
            pos,
            size: ImVec2::new(0.0,0.0),
            dragged: false
        }
    }

    fn get_input_slot_pos(&self, slot: usize) -> ImVec2 {
        ImVec2{
            x: self.pos.x,
            y: self.pos.y + (slot as f32 + 1.3) * MODULE_LINE_HEIGHT + NODE_WINDOW_PADDING.y
        }
    }

    fn get_output_slot_pos(&self, slot: usize) -> ImVec2 {
        ImVec2{
            x: self.pos.x + self.size.x,
            y: self.pos.y + (slot as f32 + 1.3) * MODULE_LINE_HEIGHT + NODE_WINDOW_PADDING.y
        }
    }
}

const IO_NAME_LENGTH: usize = 64;
type IOChar = u8;

fn slice_to_str(slice: &[IOChar; IO_NAME_LENGTH]) -> &str {
    for i in 0..IO_NAME_LENGTH {
        if slice[i] == 0 {
            return str::from_utf8(&slice[0..i]).unwrap()
        }
    }
    str::from_utf8(slice.as_ref()).unwrap()
}

struct InputModule {
    new_entry: [IOChar; IO_NAME_LENGTH],
    inputs: Vec<[IOChar; IO_NAME_LENGTH]>
}

impl InputModule {
    fn new() -> InputModule {
        InputModule {
            new_entry: [0; IO_NAME_LENGTH],
            inputs: vec![],
        }
    }
}

impl Module for InputModule {
    fn name(&self) -> &'static str {
        "Input"
    }

    fn outputs(&self) -> Vec<&ModuleOutputName> {
        AsRef::<Vec<[IOChar; IO_NAME_LENGTH]>>::as_ref(&self.inputs).into_iter().map(slice_to_str).collect()
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            let mut to_delete = None;
            for i in 0..self.inputs.len() {
                let label = CString::new(i.to_string()).unwrap();
                cimgui::igPushIDInt(i as i32);
                cimgui::igInputText(label.as_ptr(), self.inputs[i].as_mut_ptr() as *mut i8, self.inputs[i].len(), 0, None, nullptr());
                cimgui::igSameLine(0.0, -1.0);
                if cimgui::igButton(cstr!("Del"), ImVec2::new(0.0,0.0)) {
                    to_delete = Some(i);
                }
                cimgui::igPopID();
            }
            if let Some(i_delete) = to_delete {
                self.inputs.remove(i_delete);
            }
            cimgui::igInputTextWithHint(cstr!(""), cstr!("New input name"), self.new_entry.as_mut_ptr() as *mut i8, self.new_entry.len(), 0, None, nullptr());
            if self.new_entry[0] != 0 {
                cimgui::igSameLine(0.0, -1.0);
                if cimgui::igButton(cstr!("Add"), ImVec2::new(0.0,0.0)) {
                    self.inputs.push(self.new_entry);
                    self.new_entry = [0; IO_NAME_LENGTH];
                }
            }
        }
    }
}

struct OutputModule {
    new_entry: [IOChar; IO_NAME_LENGTH],
    outputs: Vec<[IOChar; IO_NAME_LENGTH]>
}

impl OutputModule {
    fn new() -> OutputModule {
        OutputModule {
            new_entry: [0; IO_NAME_LENGTH],
            outputs: vec![],
        }
    }

    fn add(&mut self, name: &ModuleIOName) {
        if name.len() >= IO_NAME_LENGTH{
            panic!("Name too long");
        }
        let mut output = [0;IO_NAME_LENGTH];
        let bytes = name.as_bytes();
        for i in 0..bytes.len() {
            output[i] = bytes[i];
        }
        self.outputs.push(output);
    }
}

impl Module for OutputModule {
    fn name(&self) -> &'static str {
        "Output"
    }

    fn inputs(&self) -> Vec<&ModuleOutputName> {
        AsRef::<Vec<[IOChar; IO_NAME_LENGTH]>>::as_ref(&self.outputs).into_iter().map(slice_to_str).collect()
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            let mut to_delete = None;
            for i in 0..self.outputs.len() {
                let label = CString::new(i.to_string()).unwrap();
                cimgui::igPushIDInt(i as i32);
                cimgui::igInputText(label.as_ptr(), self.outputs[i].as_mut_ptr() as *mut i8, self.outputs[i].len(), 0, None, nullptr());
                cimgui::igSameLine(0.0, -1.0);
                if cimgui::igButton(cstr!("Del"), ImVec2::new(0.0,0.0)) {
                    to_delete = Some(i);
                }
                cimgui::igPopID();
            }
            if let Some(i_delete) = to_delete {
                self.outputs.remove(i_delete);
            }
            cimgui::igInputTextWithHint(cstr!(""), cstr!("New output name"), self.new_entry.as_mut_ptr() as *mut i8, self.new_entry.len(), 0, None, nullptr());
            if self.new_entry[0] != 0 {
                cimgui::igSameLine(0.0, -1.0);
                if cimgui::igButton(cstr!("Add"), ImVec2::new(0.0,0.0)) {
                    self.outputs.push(self.new_entry);
                    self.new_entry = [0; IO_NAME_LENGTH];
                }
            }
        }
    }
}

struct ModuleInfo {
    module: Box<dyn Module>,
    ui: ModuleUI,
    output_products: Voices,
    input_connections: Vec<Option<OutputConnector>>,
    updated: bool,
}

impl ModuleInfo {
    fn new(module: Box<dyn Module>, position: ImVec2) -> ModuleInfo {
        let output_size = module.outputs().len();
        let input_size = module.inputs().len();
        ModuleInfo {
            module,
            ui: ModuleUI::new(position),
            output_products: Voices::new(output_size),
            input_connections: vec![None; input_size],
            updated: false
        }
    }

    fn update_io_sizes(&mut self) {
        let input_size = self.module.inputs().len();
        self.input_connections.resize(input_size, None);
        let output_size = self.module.outputs().len();
        self.output_products.set_io_size(output_size);

    }

    fn connect(&mut self, input_slot: usize, output_connector: OutputConnector) {
        self.update_io_sizes();
        self.input_connections[input_slot] = Some(output_connector);
    }

    fn disconnect_from_module(&mut self, id: &ModuleID) {
        for i in 0..self.input_connections.len() {
            match &self.input_connections[i] {
                Some(connector) => {
                    if connector.node == *id {
                        self.input_connections[i] = None
                    }
                }
                _ => {}
            }
        }
    }

    fn get_dependencies(&self) -> Vec<ModuleID> {
        tracy_scope!();
        let mut modules = vec![];
        for option in &self.input_connections {
            match option {
                Some(output) => {
                    modules.push(output.node);
                }
                _ => {}
            }
        }
        modules
    }

    fn update(&mut self, context: &mut UpdateContext, inputs: &Voices) -> &Voices {
        tracy_scope!();
        if !self.updated {
            self.output_products = self.module.update_voices(context, inputs);
            self.updated = true;
        }
        &self.output_products
    }
}

pub struct Program {
    modules: HashMap<ModuleID, ModuleInfo>,

    module_draw_order: Vec<ModuleID>,
    scrolling: ImVec2,
    node_selected: Option<ModuleID>,
    context_menu_node: Option<ModuleID>,
    context_menu_link: Option<Connection>,
    context_menu_position: Option<ImVec2>,
    link_splitter: *mut ImDrawListSplitter,
    node_splitter: *mut ImDrawListSplitter,
    node_ui_splitter: *mut ImDrawListSplitter,

    next_id: ModuleID,
    pub window_opened: bool,
    uuid: UUID,
}

pub const INPUT_MODULE_ID: ModuleID = 0;
pub const OUTPUT_MODULE_ID: ModuleID = 1;

impl Program {
    pub fn new() -> Program {
        let mut modules = HashMap::new();
        modules.insert(INPUT_MODULE_ID, ModuleInfo::new(Box::from(InputModule::new()), ImVec2::new(0.0,0.0)));
        modules.insert(OUTPUT_MODULE_ID, ModuleInfo::new(Box::from(OutputModule::new()), ImVec2::new(800.0,0.0)));
        let program = Program {
            modules,
            module_draw_order: vec![INPUT_MODULE_ID,OUTPUT_MODULE_ID],
            scrolling: ImVec2::new(0.0,0.0),
            node_selected: None,
            context_menu_node: None,
            context_menu_link: None,
            context_menu_position: None,
            link_splitter: unsafe {cimgui::ImDrawListSplitter_ImDrawListSplitter()},
            node_splitter: unsafe {cimgui::ImDrawListSplitter_ImDrawListSplitter()},
            node_ui_splitter: unsafe {cimgui::ImDrawListSplitter_ImDrawListSplitter()},
            next_id: 2,
            window_opened: true,
            uuid: new_uuid(),
        };
        program
    }

    fn name() -> &'static str {
        "Program"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Program::name(), Box::new(||{Box::from(Program::new())}));
    }

    fn reset_update_status(&mut self) {
        for (_, module) in &mut self.modules {
            module.updated = false;
            module.update_io_sizes();
        }
    }

    pub fn add_output(&mut self, name: &ModuleIOName) {
        unsafe {
            let output_module = (&mut *self.modules.get_mut(&OUTPUT_MODULE_ID).unwrap().module as *mut dyn Module as *mut OutputModule).as_mut().unwrap();
            output_module.add(name);
        }
    }

    pub fn add(&mut self, module: Box<dyn Module>, position: ImVec2) -> ModuleID {
        let id = self.next_id;
        self.next_id += 1;
        self.modules.insert(id, ModuleInfo::new(module, position));
        self.module_draw_order.push(id);
        id
    }

    pub fn remove(&mut self, id: &ModuleID) {
        self.modules.remove(id);
        for (_, module_info) in &mut self.modules {
            module_info.disconnect_from_module(id);
        }
        let pos = self.module_draw_order.iter().position(|x|x == id).unwrap();
        self.module_draw_order.remove(pos);
    }

    fn is_module_dependent(&self, module_id: &ModuleID, dependency: &ModuleID) -> bool {
        tracy_scope!();
        let dependencies = self.modules[module_id].get_dependencies();
        for m in &dependencies {
            if m == dependency {
                return true
            }
        }
        for m in &dependencies {
            if self.is_module_dependent(m, dependency) {
                return true
            }
        }
        false
    }

    pub fn connect(&mut self, connection: &Connection) {
        if self.is_module_dependent(&connection.source.node, &connection.destination.node) {
            return
        }
        self.modules.get_mut(&connection.destination.node).unwrap().connect(connection.destination.slot, connection.source);
    }

    pub fn disconnect(&mut self, module_id: &ModuleID, slot: usize) {
        self.modules.get_mut(module_id).unwrap().input_connections[slot] = None;
    }

    pub fn update_module_and_dependencies(&mut self, context: &mut UpdateContext, module_id: &ModuleID) -> &Voices {
        tracy_scope!();
        // TODO clean up
        let module_info = self.modules.get(module_id).unwrap();
        if !module_info.updated {
            let input = self.compute_inputs_for_module(context, module_id);
            let module_info = self.modules.get_mut(module_id).unwrap();
            module_info.update(context, &input);
        }
        let module_info = self.modules.get(module_id).unwrap();
        &module_info.output_products
    }

    pub fn compute_inputs_for_module(&mut self, context: &mut UpdateContext, module_id: &ModuleID) -> Voices {
        tracy_scope!();
        // TODO clean up
        let module_input_size = self.modules.get(module_id).unwrap().input_connections.len();
        let mut broadcast_buffers = vec![None;module_input_size];
        let mut has_broadcast = false;
        let mut has_no_voice_connection = false;
        let mut has_voice_connection = false;
        let mut inputs = Voices::new(module_input_size);
        for i in 0..module_input_size {
            let module_info = self.modules.get(module_id).unwrap();
            match &module_info.input_connections[i] {
                Some(connection) => {
                    let node = connection.node;
                    let slot = connection.slot;
                    let outputs = self.update_module_and_dependencies(context, &node);
                    if outputs.is_empty() {
                        has_no_voice_connection = true;
                    }
                    if outputs.contains_key(&0) {
                        has_broadcast = true;
                        broadcast_buffers[i] = Some(outputs.get(&0).unwrap()[slot]);
                    } else {
                        if !outputs.is_empty() {
                            has_voice_connection = true;
                        }
                        inputs.insert_from_voices(i, outputs, slot);
                    }
                }
                _ => {}
            }
        }
        if has_broadcast && has_no_voice_connection && !has_voice_connection {
            return Voices::new(module_input_size);
        }
        if has_broadcast && inputs.is_empty() {
            let mut input_buffers = vec![StereoBuffer::constant(0.0);module_input_size];
            for i in 0..broadcast_buffers.len() {
                let buffer = broadcast_buffers[i];
                match buffer {
                    None => {}
                    Some(buffer) => {
                        input_buffers[i] = buffer;
                    }
                }
            }
            inputs.insert(0, input_buffers);
        } else {
            for i in 0..broadcast_buffers.len() {
                let buffer = broadcast_buffers[i];
                match buffer {
                    None => {}
                    Some(buffer) => {
                        for (_, input_buffers) in inputs.deref_mut() {
                            input_buffers[i] = buffer;
                        }
                    }
                }
            }
        }
        inputs
    }

    fn get_links(&self) -> Vec<Connection> {
        let mut links = Vec::new();
        for (module_id, module_info) in &self.modules {
            for slot in 0..module_info.input_connections.len() {
                match &module_info.input_connections[slot] {
                    Some(output) => {
                        links.push(Connection::new(output.clone(),InputConnector::new(*module_id, slot)))
                    }
                    None => {}
                }
            }
        }
        links
    }

    pub fn draw_window(&mut self) {
        tracy_scope!();
        unsafe {
            if self.window_opened {
                let mut window_id = "Program##".to_owned();
                window_id.push_str(&self.uuid.to_string());
                let window_id = CString::new(window_id).unwrap();
                if cimgui::igBegin(window_id.as_ptr(), &mut self.window_opened, 0) {
                    let io = cimgui::igGetIO().as_mut().unwrap();
                    let style = igGetStyle().as_ref().unwrap();

                    cimgui::igBeginGroup();
                    cimgui::igPushStyleVarVec2(cimgui::ImGuiStyleVar_FramePadding as i32, ImVec2 { x: 1.0, y: 1.0 });
                    cimgui::igPushStyleVarVec2(cimgui::ImGuiStyleVar_WindowPadding as i32, ImVec2 { x: 0.0, y: 0.0 });
                    cimgui::igBeginChildID(self.uuid, ImVec2 { x: 0.0, y: 0.0 }, true, (cimgui::ImGuiWindowFlags_NoScrollbar | cimgui::ImGuiWindowFlags_NoMove) as i32);
                    cimgui::igPopStyleVar(1);
                    cimgui::igPushItemWidth(120.0);

                    let draw_list = cimgui::igGetWindowDrawList();
                    let offset = imgui::get_cursor_screen_pos() + self.scrolling;

                    cimgui::ImDrawListSplitter_Split(self.link_splitter, draw_list, 2);
                    cimgui::ImDrawListSplitter_SetCurrentChannel(self.link_splitter, draw_list, 1);

                    // For each node
                    let mut node_hovered = None;
                    cimgui::ImDrawListSplitter_Split(self.node_splitter, draw_list, self.module_draw_order.len() as i32);
                    let mut connector_hovered = false;
                    for i in (0..self.module_draw_order.len()).rev() {
                        tracy_scope_named!("node");
                        cimgui::ImDrawListSplitter_SetCurrentChannel(self.node_splitter, draw_list, i as i32);
                        let node_id = self.module_draw_order[i];
                        //self.draw_module(node_id);
                        let node_rect_min;
                        let node_rect_max;
                        let old_any_active;

                        {
                            let module_info = self.modules.get_mut(&node_id).unwrap();
                            if module_info.ui.dragged {
                                module_info.ui.pos = module_info.ui.pos + io.MouseDelta;
                            }

                            cimgui::ImDrawListSplitter_Split(self.node_ui_splitter, draw_list, 2);
                            cimgui::igPushIDInt(node_id as i32);
                            node_rect_min = offset + module_info.ui.pos;

                            cimgui::ImDrawListSplitter_SetCurrentChannel(self.node_ui_splitter, draw_list, 1);

                            old_any_active = cimgui::igIsAnyItemActive();
                            cimgui::igSetCursorScreenPos(node_rect_min + NODE_WINDOW_PADDING);
                            cimgui::igBeginGroup();
                            let name = CString::new(module_info.module.name()).unwrap();
                            cimgui::igText(name.as_ptr());
                            module_info.module.draw_gui();
                            cimgui::igEndGroup();

                            let mut size = imgui::get_item_rect_size();
                            let size_y = size.y.max(
                                ((module_info.module.inputs().len() as f32 + 0.7) * MODULE_LINE_HEIGHT).max(
                                    (module_info.module.outputs().len() as f32 + 0.7) * MODULE_LINE_HEIGHT));
                            size.y = size_y;
                            module_info.ui.size = size + NODE_WINDOW_PADDING + NODE_WINDOW_PADDING;
                            node_rect_max = node_rect_min + module_info.ui.size;
                        }
                        let node_widgets_active = !old_any_active && cimgui::igIsAnyItemActive();


                        // Connectors handling
                        let inputs_len = self.modules.get(&node_id).unwrap().module.inputs().len();

                        for slot_id in 0..inputs_len {
                            tracy_scope_named!("input");
                            let mut input_id = "input_".to_owned();
                            input_id.push_str(&*slot_id.to_string());
                            let input_id = CString::new(input_id).unwrap();
                            let center = offset + self.modules.get(&node_id).unwrap().ui.get_input_slot_pos(slot_id);
                            cimgui::igSetCursorScreenPos(center + ImVec2::new(-NODE_SLOT_RADIUS * 2.0, -NODE_SLOT_RADIUS * 2.0));
                            cimgui::igInvisibleButton(input_id.into_raw(), ImVec2::new(NODE_SLOT_RADIUS * 4.0, NODE_SLOT_RADIUS * 4.0), 0);
                            if cimgui::igIsItemHovered(0) {
                                connector_hovered = true;
                                cimgui::ImDrawList_AddCircle(draw_list, center, NODE_SLOT_RADIUS * 1.5, 0xFF969696, 0, 1.0);
                                cimgui::igBeginTooltip();
                                let slot_name_raw = CString::new(self.modules.get(&node_id).unwrap().module.inputs()[slot_id]).unwrap();
                                cimgui::igText(slot_name_raw.as_ptr());
                                cimgui::igEndTooltip();
                            }
                            if cimgui::igIsItemActive() && cimgui::igBeginDragDropSource(0) {
                                let payload = DraggedConnector::input(self.uuid, node_id, slot_id);
                                cimgui::igSetDragDropPayload(cstr!("input"), &payload as *const DraggedConnector as *const c_void, sizeof::<DraggedConnector>(), 0);
                                cimgui::igEndDragDropSource();
                            }
                            if cimgui::igBeginDragDropTarget() {
                                let payload = cimgui::igAcceptDragDropPayload(cstr!("output"), 0).as_ref();
                                if let Some(p) = payload {
                                    let connector = (p.Data as *mut DraggedConnector).as_ref().unwrap();
                                    if connector.program_uuid == self.uuid {
                                        self.connect(&Connection::new2(connector.node, connector.slot, node_id, slot_id));
                                    }
                                }
                                cimgui::igEndDragDropTarget()
                            }
                        }

                        let outputs_len = self.modules.get(&node_id).unwrap().module.outputs().len();
                        for slot_id in 0..outputs_len {
                            tracy_scope_named!("output");
                            let mut output_id = "output_".to_owned();
                            output_id.push_str(&*slot_id.to_string());
                            let output_id = CString::new(output_id).unwrap();
                            let center = offset + self.modules.get(&node_id).unwrap().ui.get_output_slot_pos(slot_id);
                            cimgui::igSetCursorScreenPos(center + ImVec2::new(-NODE_SLOT_RADIUS * 2.0, -NODE_SLOT_RADIUS * 2.0));
                            cimgui::igInvisibleButton(output_id.into_raw(), ImVec2::new(NODE_SLOT_RADIUS * 4.0, NODE_SLOT_RADIUS * 4.0), 0);
                            if cimgui::igIsItemHovered(0) {
                                connector_hovered = true;
                                cimgui::ImDrawList_AddCircle(draw_list, center, NODE_SLOT_RADIUS * 1.5, 0xFF969696, 0, 1.0);
                                cimgui::igBeginTooltip();
                                let slot_name_raw = CString::new(self.modules.get(&node_id).unwrap().module.outputs()[slot_id]).unwrap();
                                cimgui::igText(slot_name_raw.as_ptr());
                                cimgui::igEndTooltip();
                            }
                            if cimgui::igIsItemActive() && cimgui::igBeginDragDropSource(0) {
                                let payload = DraggedConnector::output(self.uuid, node_id, slot_id);
                                cimgui::igSetDragDropPayload(cstr!("output"), &payload as *const DraggedConnector as *const c_void, sizeof::<DraggedConnector>(), 0);
                                cimgui::igEndDragDropSource();
                            }
                            if cimgui::igBeginDragDropTarget() {
                                let payload = cimgui::igAcceptDragDropPayload(cstr!("input"), 0).as_ref();
                                if let Some(p) = payload {
                                    let connector = (p.Data as *mut DraggedConnector).as_ref().unwrap();
                                    if connector.program_uuid == self.uuid {
                                        self.connect(&Connection::new2(node_id, slot_id, connector.node, connector.slot));
                                    }
                                }
                                cimgui::igEndDragDropTarget()
                            }
                        }

                        // Main node
                        let module_info = self.modules.get_mut(&node_id).unwrap();
                        cimgui::ImDrawListSplitter_SetCurrentChannel(self.node_ui_splitter, draw_list, 0);
                        cimgui::igSetCursorScreenPos(node_rect_min);
                        cimgui::igInvisibleButton(cstr!("node"), module_info.ui.size, 0);
                        if !connector_hovered && cimgui::igIsItemHovered(0) && node_hovered.is_none() {
                            node_hovered = Some(node_id);
                        }
                        let node_moving_active = cimgui::igIsItemActive();
                        if node_widgets_active || node_moving_active {
                            self.node_selected = Some(node_id);
                        }
                        if node_moving_active && cimgui::igIsMouseDragging(cimgui::ImGuiMouseButton_Left as i32, -1.0)
                        {
                            if !module_info.ui.dragged {
                                module_info.ui.pos = module_info.ui.pos + io.MouseDelta;
                            }
                            module_info.ui.dragged = true;
                        } else {
                            module_info.ui.dragged = false;
                        }

                        // Node drawing
                        let node_bg_color = if node_hovered == Some(node_id) || self.context_menu_node == Some(node_id) {
                            0xFF484848
                        } else {
                            0xFF3C3C3C
                        };
                        cimgui::ImDrawList_AddRectFilled(draw_list, node_rect_min, node_rect_max, node_bg_color, 4.0, cimgui::ImDrawCornerFlags_None as i32);
                        cimgui::ImDrawList_AddRect(draw_list, node_rect_min, node_rect_max, 0xFF646464, 4.0, cimgui::ImDrawCornerFlags_None as i32, 1.0);

                        // Connectors drawing
                        for slot in 0..inputs_len {
                            let center = (offset) + module_info.ui.get_input_slot_pos(slot);
                            cimgui::ImDrawList_AddCircleFilled(draw_list, center, NODE_SLOT_RADIUS, 0xFF969696, 0);
                        }
                        for slot in 0..outputs_len {
                            let center = (offset) + module_info.ui.get_output_slot_pos(slot);
                            cimgui::ImDrawList_AddCircleFilled(draw_list, center, NODE_SLOT_RADIUS, 0xFF969696, 0);
                        }

                        cimgui::igPopID();
                        cimgui::ImDrawListSplitter_Merge(self.node_ui_splitter, draw_list);
                    }
                    cimgui::ImDrawListSplitter_Merge(self.node_splitter, draw_list);


                    // Links drawing
                    let mut link_hovered = None;
                    cimgui::ImDrawListSplitter_SetCurrentChannel(self.link_splitter, draw_list, 0);
                    let links = self.get_links();
                    for link_i in 0..links.len() {
                        tracy_scope_named!("link");
                        let link = links.get(link_i).unwrap().to_owned();
                        let node_in = &self.modules.get(&link.source.node).unwrap().ui;
                        let node_out = &self.modules.get(&link.destination.node).unwrap().ui;
                        let p1 = offset + node_in.get_output_slot_pos(link.source.slot);
                        let p4 = offset + node_out.get_input_slot_pos(link.destination.slot);
                        let p2 = p1 + ImVec2 { x: 50.0, y: 0.0 };
                        let p3 = p4 + ImVec2 { x: -50.0, y: 0.0 };
                        let mut closest = ImVec2::new(0.0, 0.0);
                        cimgui::igImBezierClosestPointCasteljau(&mut closest, p1, p2, p3, p4, io.MousePos, style.CurveTessellationTol);
                        if link_hovered.is_none() && !connector_hovered && node_hovered == None && cimgui::igIsWindowHovered(0) && closest.distance(&(io.MousePos)) < 5.0 {
                            link_hovered = Some(link);
                        }
                        let mut thickness = 3.0;
                        if let Some(selected) = &self.context_menu_link {
                            if link == *selected {
                                thickness = 5.0;
                            }
                        } else if let Some(selected) = &link_hovered {
                            if link == *selected {
                                thickness = 5.0;
                            }
                        }
                        let mut color = 0xFF666666;
                        let voices = &self.modules.get(&link.source.node).unwrap().output_products;
                        if voices.contains_key(&0) {
                            color = 0xFFFFFFFF;
                        }
                        else if !voices.is_empty() {
                            color = 0xFFC8C864;
                        }
                        cimgui::ImDrawList_AddBezierCurve(draw_list, p1, p2, p3, p4, color, thickness, 0);
                    }


                    // Link dragged drawing
                    if let Some(linking) = cimgui::igGetDragDropPayload().as_ref() {
                        let connector = (linking.Data as *const DraggedConnector).as_ref().unwrap();
                        if connector.program_uuid == self.uuid {
                            match connector.kind {
                                ConnectorKind::Input => {
                                    let module_info = self.modules.get(&connector.node).unwrap();
                                    let p1 = io.MousePos;
                                    let p2 = offset + module_info.ui.get_input_slot_pos(connector.slot);
                                    cimgui::ImDrawList_AddBezierCurve(draw_list, p1, p1 + ImVec2 { x: 50.0, y: 0.0 }, p2 + ImVec2 { x: -50.0, y: 0.0 }, p2, 0xFFC8C864, 3.0, 0);
                                }
                                ConnectorKind::Output => {
                                    let module_info = self.modules.get(&connector.node).unwrap();
                                    let p1 = offset + module_info.ui.get_output_slot_pos(connector.slot);
                                    let p2 = io.MousePos;
                                    cimgui::ImDrawList_AddBezierCurve(draw_list, p1, p1 + ImVec2 { x: 50.0, y: 0.0 }, p2 + ImVec2 { x: -50.0, y: 0.0 }, p2, 0xFFC8C864, 3.0, 0);
                                }
                            }
                        }
                    }
                    cimgui::ImDrawListSplitter_Merge(self.link_splitter, draw_list);


                    // Selected node goes on top
                    if self.node_selected.is_some() && self.module_draw_order[self.module_draw_order.len() - 1] != self.node_selected.unwrap() {
                        let mut idx = None;
                        for i in 0..self.module_draw_order.len() {
                            if self.node_selected.is_some() && self.module_draw_order[i] == self.node_selected.unwrap() {
                                idx = Some(i);
                            }
                        }
                        if let Some(idx) = idx {
                            self.module_draw_order.remove(idx);
                            self.module_draw_order.push(self.node_selected.unwrap());
                        }
                    }


                    // Open context menu
                    if cimgui::igIsMouseReleased(cimgui::ImGuiMouseButton_Right) && cimgui::igIsWindowHovered(0) {
                        self.context_menu_node = node_hovered;
                        self.context_menu_link = link_hovered;
                        let mut window_pos = ImVec2::new(0.0, 0.0);
                        cimgui::igGetWindowPos(&mut window_pos);
                        self.context_menu_position = Some(io.MousePos - window_pos);
                        cimgui::igOpenPopup(cstr!("context_menu"), 0);
                    }

                    if cimgui::igBeginPopup(cstr!("context_menu"), cimgui::ImGuiWindowFlags_NoMove) {
                        if self.context_menu_node.is_some() {
                            if cimgui::igMenuItemBool(cstr!("Delete node"), nullptr(), false, self.context_menu_node.unwrap() > 1) {
                                self.remove(&self.context_menu_node.unwrap());
                            }
                        } else if let Some(link) = self.context_menu_link {
                            if cimgui::igMenuItemBool(cstr!("Delete link"), nullptr(), false, true) {
                                self.disconnect(&link.destination.node, link.destination.slot);
                            }
                        } else {
                            let registry = REGISTRY.as_ref().unwrap();
                            let modules = registry.get_registered_modules();
                            for module in modules {
                                let module_name = CString::new(module).unwrap();
                                if cimgui::igMenuItemBool(module_name.as_ptr(), nullptr(), false, true)
                                {
                                    let m = registry.new_module(module);
                                    self.add(m, self.context_menu_position.unwrap() - self.scrolling);
                                }
                            }
                        }
                        if cimgui::igIsMouseReleased(cimgui::ImGuiMouseButton_Left) ||
                            cimgui::igIsMouseClicked(cimgui::ImGuiMouseButton_Right, false) {
                            cimgui::igCloseCurrentPopup();
                        }
                        cimgui::igEndPopup();
                    }

                    if !cimgui::igIsPopupOpenStr(cstr!("context_menu"), 0) {
                        self.context_menu_link = None;
                        self.context_menu_node = None;
                    }


                    // Scrolling
                    if cimgui::igIsWindowHovered(0) && !cimgui::igIsAnyItemActive() && cimgui::igIsMouseDragging(cimgui::ImGuiMouseButton_Middle as i32, 0.0)
                    {
                        self.scrolling = self.scrolling + io.MouseDelta;
                    }


                    cimgui::igPopItemWidth();
                    cimgui::igEndChild();
                    cimgui::igPopStyleVar(1);
                    cimgui::igEndGroup();
                }
                cimgui::igEnd();
            }
        }
    }

}

impl Module for Program {
    fn name(&self) -> &'static str {
        Self::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        self.modules.get(&INPUT_MODULE_ID).unwrap().module.outputs()
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        self.modules.get(&OUTPUT_MODULE_ID).unwrap().module.inputs()
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        self.reset_update_status();
        let mut input_module = self.modules.get_mut(&INPUT_MODULE_ID).unwrap();
        input_module.output_products = inputs.clone();
        input_module.updated = true;
        let keys: Vec<ModuleID> = self.modules.keys().cloned().collect();
        for module_id in keys {
            if module_id != OUTPUT_MODULE_ID && module_id != INPUT_MODULE_ID {
                let module_info = self.modules.get(&module_id).unwrap();
                if !module_info.updated {
                    let inputs = self.compute_inputs_for_module(context, &module_id);
                    let module_info = self.modules.get_mut(&module_id).unwrap();
                    module_info.update(context, &inputs);
                }
            }
        }
        self.compute_inputs_for_module(context, &OUTPUT_MODULE_ID)
    }



    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            if cimgui::igButton(cstr!("Open Window"),ImVec2::new(0.0,0.0)) {
                self.window_opened = true;
            }
            self.draw_window();
        }
    }
}