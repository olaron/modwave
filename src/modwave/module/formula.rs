use crate::modwave::{Sample, Registry, StereoSample, UpdateContext};
use crate::modwave::module::{Module, ModuleIOName, VoiceID, Voices};
use crate::c::nullptr;
use cimgui::ImVec2;
use std::ffi::CString;
use fasteval::{Compiler, Instruction, Evaler};
use std::collections::{BTreeSet, BTreeMap};
use std::ops::Deref;

const FORMULA_LENGTH: usize = 128;

pub struct Formula {
    entry: [i8; FORMULA_LENGTH],
    compiled_formula: String,
    parser: fasteval::Parser,
    slab: fasteval::Slab,
    compiled: Option<Instruction>,
    compilation_status: String,
    variables: BTreeMap<String,Sample>,
    input_list: Vec<String>,
}

impl Formula {
    pub fn new(formula: String) -> Formula {
        Formula {
            entry: [0;FORMULA_LENGTH],
            compiled_formula: formula,
            parser: fasteval::Parser::new(),
            slab: fasteval::Slab::new(),
            compiled: None,
            compilation_status: "OK".to_string(),
            variables: BTreeMap::new(),
            input_list: Vec::new(),
        }
    }

    fn name() -> &'static str {
        "Formula"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Formula::name(), Box::new(||{Box::from(Formula::new("0".to_string()))}));
    }

    fn entry_length(&self) -> usize {
        for i in 0..FORMULA_LENGTH-1 {
            if self.entry[i] == 0 {
                return i
            }
        }
        return FORMULA_LENGTH
    }

    fn prepare_inputs(&mut self, variables: BTreeSet<String>) {
        self.input_list.clear();
        self.variables.clear();
        for variable in &variables {
            self.input_list.push(variable.clone());
            self.variables.insert(variable.clone(), 0.0);
        }
        self.input_list.sort();
    }

    fn compile(&mut self) {
        let length = self.entry_length();
        let formula: String;
        unsafe {
            formula = String::from_utf8_lossy(&(std::slice::from_raw_parts(&self.entry as *const i8 as *const u8, length))).parse().unwrap();
        }
        let parsed = self.parser.parse(formula.as_str(),&mut self.slab.ps);
        match parsed {
            Ok(expression) => {
                let expression = expression.from(&self.slab.ps);
                let compiled = expression.compile(&self.slab.ps, &mut self.slab.cs);
                let variables = expression.var_names(&self.slab);
                self.prepare_inputs(variables);
                self.compiled_formula = formula;
                self.compiled = Some(compiled);
                self.compilation_status = "OK".to_string();
            }
            Err(error) => {
                self.compilation_status = error.to_string();
            }
        }
    }

}

impl Module for Formula {
    fn name(&self) -> &'static str {
        Formula::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        let mut inputs = vec![];
        for variable in &self.input_list {
            inputs.push(variable.as_str());
        }
        inputs.sort();
        inputs
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["output"]
    }

    fn update_sample(&mut self,
                     inputs: &Vec<StereoSample>,
                     _voice_id: &VoiceID) -> Vec<StereoSample> {
        tracy_scope!();
        let mut result = StereoSample::from_sample(0.0);
        if let Some(compiled) = &self.compiled {
            for i in 0..self.input_list.len() {
                self.variables.insert(self.input_list[i].clone(),inputs[i].left);
            }
            result.left = compiled.eval(&self.slab, &mut self.variables).unwrap();

            for i in 0..self.input_list.len() {
                self.variables.insert(self.input_list[i].clone(),inputs[i].right);
            }
            result.right = compiled.eval(&self.slab, &mut self.variables).unwrap();
        }
        vec![result]
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut output_voices = Voices::new(self.outputs().len());
        if self.inputs().len() == 0 {
            let output_buffers = self.update_buffer(context, &vec![], &0);
            output_voices.insert(0, output_buffers);
        }
        else {
            for (voice_id, input_buffers) in inputs.deref() {
                let output_buffers = self.update_buffer(context, input_buffers, voice_id);
                output_voices.insert(*voice_id, output_buffers);
            }
        }
        output_voices
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            let cformula = CString::new(self.compiled_formula.as_bytes()).unwrap();
            cimgui::igText(cstr!("%s"), cformula.as_ptr());
            cimgui::igInputTextWithHint(cstr!(""), cstr!("(a+b)*c"), self.entry.as_mut_ptr(), FORMULA_LENGTH, 0,None,nullptr());
            if cimgui::igButton(cstr!("Compile"), ImVec2::new(0.0,0.0)) {
                self.compile();
            }
            let status = CString::new(self.compilation_status.as_bytes()).unwrap();
            cimgui::igText(status.as_ptr());
        }
    }
}