use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, StereoSample, Sample, UpdateContext, StereoBuffer};
use crate::modwave::module::Voices;

pub struct Time{
    time: usize,
}

impl Time {
    pub fn new() -> Time {
        Time {
            time: 0
        }
    }

    pub fn name() -> &'static str {
        "Time"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Time::name(), Box::new(||{Box::from(Time::new())}));
    }

    fn sample(&mut self, sample_rate: Sample) -> StereoSample {
        tracy_scope!();
        let t = self.time as Sample;
        self.time += 1;
        StereoSample::from_sample(t/sample_rate)
    }
}

impl Module for Time {
    fn name(&self) -> &'static str {
        Time::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec![]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["time"]
    }

    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     _: &Vec<StereoBuffer>,
                     _: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let mut output_buffers = vec![context.store.allocate_stereo(context.length)];
        for i in 0..context.length {
            output_buffers[0].set(i, self.sample(context.sample_rate as Sample));
        }
        output_buffers
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     _: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut output_voices = Voices::new(1);
        output_voices.insert(0,  self.update_buffer(context, &vec![], &0));
        output_voices
    }
}