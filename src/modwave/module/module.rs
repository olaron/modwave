use super::super::bufferstore::*;

use std::collections::{HashMap};
use std::ops::{Deref, DerefMut};
use crate::modwave::UpdateContext;

pub type ModuleIOName = str;
pub type ModuleInputName = ModuleIOName;
pub type ModuleOutputName = ModuleIOName;

pub type VoiceID = u32;

#[derive(Clone, Debug)]
pub struct Voices {
    voices: HashMap<VoiceID, Vec<StereoBuffer>>,
    n_io: usize,
}

impl Voices {
    pub fn new(n_io: usize) -> Voices {
        Voices {
            voices: HashMap::new(),
            n_io,
        }
    }

    pub fn set_io_size(&mut self, n_io: usize) {
        self.n_io = n_io;
    }

    fn new_voice(&mut self, voice_id: &VoiceID) {
        let buffers = vec![StereoBuffer::constant(0.0); self.n_io];
        self.voices.insert(*voice_id, buffers);
    }

    pub fn insert_from_voices(&mut self, i: usize, voices: &Voices, voices_i: usize) {
        for (voice_id, buffers) in voices.deref() {
            if !self.voices.contains_key(voice_id) {
                self.new_voice(voice_id);
            }
            self.voices.get_mut(voice_id).unwrap()[i] = buffers[voices_i]
        }
    }

    pub fn to_16bit_stereo(&self, length: usize) -> Vec<i16> {
        let mut buffers = Vec::with_capacity(length*2);
        for voice in self.voices.values() {
            buffers.append(voice.clone().as_mut());
        }
        let mut output = Vec::with_capacity(length*2);
        for i in 0..length {
            let mut left = 0.0;
            let mut right = 0.0;
            for b in &buffers {
                left += b.at(i).left;
                right += b.at(i).right;
            }
            left = (left * 2000.0).min(32767.0).max(-32768.0).round();
            right = (right * 2000.0).min(32767.0).max(-32768.0).round();
            output.push(left as i16);
            output.push(right as i16);
        }
        output
    }

    #[allow(dead_code)]
    fn debug_string(&self) -> String {
        let mut string = "Voices {".to_string();
        for key in self.voices.keys() {
            string.push_str(&key.to_string());
            string.push_str(",");
        }
        string.push_str("}");
        string
    }
}

impl Deref for Voices {
    type Target = HashMap<VoiceID, Vec<StereoBuffer>>;

    fn deref(&self) -> &Self::Target {
        &self.voices
    }
}

impl DerefMut for Voices {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.voices
    }
}


pub trait Module {
    fn name(&self) -> &'static str;

    fn inputs(&self) -> Vec<&ModuleInputName> { vec![] }
    fn outputs(&self) -> Vec<&ModuleOutputName> { vec![] }

    fn update_sample(&mut self,
                     _inputs: &Vec<StereoSample>,
                     _voice_id: &VoiceID) -> Vec<StereoSample> {
        vec![]
    }

    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     inputs_buffers: &Vec<StereoBuffer>,
                     voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let n_outputs = self.outputs().len();
        let mut output_buffers = Vec::with_capacity(n_outputs);
        for _ in 0..n_outputs {
            output_buffers.push(context.store.allocate_stereo(context.length));
        }
        for i in 0..context.length {
            tracy_scope_named!("sample");
            let mut inputs = Vec::with_capacity(inputs_buffers.len());
            for input_buffer in inputs_buffers {
                inputs.push(input_buffer.at(i));
            }
            let outputs = self.update_sample(&inputs, voice_id);
            for o in 0..outputs.len() {
                output_buffers[o].set(i,outputs[o]);
            }
        }
        output_buffers
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut output_voices = Voices::new(self.outputs().len());
        for (voice_id, input_buffers) in inputs.deref() {
            let output_buffers = self.update_buffer(context, input_buffers, voice_id);
            output_voices.insert(*voice_id, output_buffers);
        }
        output_voices
    }

    fn draw_gui(&mut self) {}
}