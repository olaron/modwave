use std::collections::{HashMap, HashSet};
use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Sample, Registry, StereoBuffer, UpdateContext};
use crate::modwave::module::Voices;
use std::ops::Deref;

struct PulseContext {
    phase: Sample,
}

impl PulseContext {
    fn new() -> PulseContext {
        PulseContext {
            phase: 0.0,
        }
    }
}

pub struct Pulse {
    context: HashMap<VoiceID, PulseContext>
}

impl Pulse {
    pub fn new() -> Pulse {
        Pulse {context: HashMap::new()}
    }

    pub fn name() -> &'static str {
        "Pulse"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Pulse::name(), Box::new(||{Box::from(Pulse::new())}));
    }
}

impl Module for Pulse {
    fn name(&self) -> &'static str {
        "Pulse"
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["frequency"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["output"]
    }

    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     inputs_buffers: &Vec<StereoBuffer>,
                     voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let sample_rate = context.sample_rate as Sample;
        let frequency = inputs_buffers[0];
        let mut result = context.store.allocate(context.length);
        if !self.context.contains_key(voice_id) {
            self.context.insert(*voice_id, PulseContext::new());
        }
        let pulse_context = self.context.get_mut(voice_id).unwrap();
        for i in 0..context.length {
            tracy_scope_named!("sample");
            let freq = frequency.left[i];
            let phase_step = freq / sample_rate;
            pulse_context.phase += phase_step;
            let sample =
                if pulse_context.phase % 2.0 < 1.0 {
                    1.0
                }
                else {
                    -1.0
                };
            result[i] = sample;
        }
        vec![StereoBuffer::new(result,result)]
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut unused_voices = HashSet::new();
        for voice in self.context.keys() {
            unused_voices.insert(*voice);
        }
        let mut output_voices = Voices::new(self.outputs().len());

        for (voice_id, input_buffers) in inputs.deref() {
            unused_voices.remove(voice_id);
            let output_buffers = self.update_buffer(context, input_buffers, voice_id);
            output_voices.insert(*voice_id, output_buffers);
        }

        for voice in unused_voices {
            self.context.remove(&voice);
        }
        output_voices
    }
}
