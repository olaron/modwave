use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, Sample, StereoSample};

pub struct Frequency {}

impl Frequency {
    pub fn new() -> Frequency {
        Frequency {}
    }

    pub fn name() -> &'static str {
        "Frequency"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Frequency::name(), Box::new(|| {Box::from(Frequency::new())}));
    }

    fn note2freq(note: Sample, root: Sample, octave: Sample) -> Sample{
        let two = 2.0 as Sample;
        root*(two.powf(note/octave))
    }
}

impl Module for Frequency {
    fn name(&self) -> &'static str {
        Frequency::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["note"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["frequency"]
    }

    fn update_sample(&mut self,
                     inputs: &Vec<StereoSample>,
                     _voice_id: &VoiceID) -> Vec<StereoSample> {
        tracy_scope!();
        vec![StereoSample::new(Frequency::note2freq(inputs[0].left, 440.0, 12.0),
                               Frequency::note2freq(inputs[0].right, 440.0, 12.0))]
    }
}