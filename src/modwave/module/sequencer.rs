use crate::modwave::module::{Module, Voices, VoiceID, new_id};
use crate::modwave::{UpdateContext, Sample, Registry, StereoBuffer};
use std::ops::Deref;
use std::collections::{HashMap, HashSet};

const MAX_LENGTH: usize = 64;

#[derive(Clone, Copy, Debug)]
struct Step {
    active: bool,
    note: Sample,
}

impl Step {
    fn new() -> Step {
        Step {
            active: false,
            note: 0.0,
        }
    }
}

pub struct Sequencer {
    length: usize,
    sequence: [Step; MAX_LENGTH],
    current_voices: HashMap<VoiceID, (usize, VoiceID)>,
}

impl Sequencer {
    pub fn new() -> Sequencer {
        Sequencer {
            length: 16,
            sequence: [Step::new(); MAX_LENGTH],
            current_voices: HashMap::new(),
        }
    }

    fn name() -> &'static str {
        "Sequencer"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Sequencer::name(), Box::new(||{Box::from(Sequencer::new())}));
    }

    fn get_current_steps(&self) -> [bool;MAX_LENGTH] {
        let mut steps = [false;MAX_LENGTH];
        for (_, (step, _)) in &self.current_voices {
            steps[*step] = true;
        }
        steps
    }
}

impl Module for Sequencer {
    fn name(&self) -> &'static str {
        Sequencer::name()
    }

    fn inputs(&self) -> Vec<&str> {
        vec!["time"]
    }

    fn outputs(&self) -> Vec<&str> {
        vec!["note"]
    }

    fn update_voices(&mut self, _: &mut UpdateContext, inputs: &Voices) -> Voices {
        let mut unused_voices = HashSet::new();
        for voice in self.current_voices.keys() {
            unused_voices.insert(*voice);
        }

        let mut output_voices = Voices::new(self.outputs().len());

        for (voice_id, input_buffers) in inputs.deref() {
            unused_voices.remove(voice_id);
            let time = input_buffers[0].left[0];
            if time >= 0.0 && time < self.length as Sample {
                let time = time as usize;
                let step  = self.sequence[time];
                let current_voice = self.current_voices.get_mut(voice_id);
                if current_voice.is_some() {
                    let c_voice = current_voice.unwrap();
                    if c_voice.0 == time && step.active {
                        output_voices.insert(c_voice.1, vec![StereoBuffer::constant(step.note)]);
                    }
                    if c_voice.0 != time && step.active {
                        c_voice.1 = new_id();
                        c_voice.0 = time;
                        output_voices.insert(c_voice.1, vec![StereoBuffer::constant(step.note)]);
                    }
                    if !step.active {
                        self.current_voices.remove(voice_id);
                    }
                }
                else {
                    if step.active {
                        let c_voice_id = new_id();
                        self.current_voices.insert(*voice_id,(time,c_voice_id));
                        output_voices.insert(c_voice_id, vec![StereoBuffer::constant(step.note)]);
                    }
                    if !step.active {
                        self.current_voices.remove(voice_id);
                    }
                }
            }
        }

        for voice in unused_voices {
            self.current_voices.remove(&voice);
        }

        output_voices
    }

    fn draw_gui(&mut self) {
        let current_steps = self.get_current_steps();
        unsafe {
            let mut length = self.length as i32;
            cimgui::igSliderInt(cstr!("Length"),&mut length,  0, MAX_LENGTH as i32, cstr!("%d"),0);
            self.length = length as usize;

            for i in 0..self.length {
                cimgui::igPushIDInt(i as i32);

                cimgui::igPushIDInt(0);
                let mut is_current = current_steps[i];
                cimgui::igCheckbox(cstr!(""), &mut is_current);
                cimgui::igPopID();

                cimgui::igSameLine(0.0,-1.0);

                cimgui::igPushIDInt(1);
                cimgui::igCheckbox(cstr!(""), &mut self.sequence[i].active);
                cimgui::igPopID();

                cimgui::igSameLine(0.0,-1.0);

                cimgui::igPushIDInt(2);
                let mut val = self.sequence[i].note as f32;
                cimgui::igDragFloat(cstr!(""), &mut val, 0.1, -100.0, 100.0, cstr!("%.2f"), 0);
                self.sequence[i].note = val as Sample;
                cimgui::igPopID();

                cimgui::igPopID();
            }
        }
    }
}