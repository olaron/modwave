use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, UpdateContext};
use crate::modwave::module::Voices;
use std::ops::Deref;
use crate::c::{nullptr, sizeof};
use cimgui::ImVec2;
use std::collections::{HashMap, VecDeque, HashSet};


const OSCILLOSCOPE_DATA_LENGTH: usize = 512;

struct OscilloscopeData {
    left: VecDeque<f32>,
    right: VecDeque<f32>,
}

impl OscilloscopeData {
    fn new() -> OscilloscopeData {
        OscilloscopeData {
            left: vec![0.0;OSCILLOSCOPE_DATA_LENGTH].into(),
            right: vec![0.0;OSCILLOSCOPE_DATA_LENGTH].into(),
        }
    }
}

pub struct Oscilloscope {
    voices: HashMap<VoiceID, OscilloscopeData>,
}

impl Oscilloscope {
    pub fn new() -> Oscilloscope {
        Oscilloscope {
            voices: HashMap::new(),
        }
    }

    pub fn name() -> &'static str {
        "Oscilloscope"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Oscilloscope::name(), Box::new(||{Box::from(Oscilloscope::new())}));
    }
}

impl Module for Oscilloscope {
    fn name(&self) -> &'static str {
        Oscilloscope::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["input"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec![]
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut unused_voices = HashSet::new();
        for voice in self.voices.keys() {
            unused_voices.insert(*voice);
        }

        for (voice_id, input_buffers) in inputs.deref() {
            unused_voices.remove(voice_id);
            if !self.voices.contains_key(voice_id) {
                self.voices.insert(*voice_id, OscilloscopeData::new());
            }

            let data = self.voices.get_mut(voice_id).unwrap();
            for i in 0..context.length {
                data.left.pop_front();
                data.left.push_back(input_buffers[0].left[i] as f32);
                data.right.pop_front();
                data.right.push_back(input_buffers[0].right[i] as f32);
            }
        }

        for voice in unused_voices {
            self.voices.remove(&voice);
        }
        Voices::new(0)
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        let mut voice_ids: Vec<VoiceID> = self.voices.keys().cloned().collect();
        voice_ids.sort();
        unsafe {
            for voice_id in voice_ids {
                let data = self.voices.get_mut(&voice_id).unwrap();
                cimgui::igText(cstr!("%d"), voice_id);
                cimgui::igPlotLinesFloatPtr(cstr!(""), data.left.make_contiguous().as_ptr(), data.left.len() as i32, 0, nullptr(), -1.0, 1.0, ImVec2::new(0.0,20.0), sizeof::<f32>() as i32);
                cimgui::igPlotLinesFloatPtr(cstr!(""), data.right.make_contiguous().as_ptr(), data.right.len() as i32, 0, nullptr(), -1.0, 1.0, ImVec2::new(0.0,20.0), sizeof::<f32>() as i32);
            }
        }
    }
}