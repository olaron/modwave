use std::collections::{HashMap, HashSet};
use std::f64::consts::PI;
use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, Sample, StereoBuffer, StereoSample, UpdateContext};
use crate::modwave::module::Voices;
use std::ops::Deref;

struct PastSamples {
    xm1: Sample,
    xm2: Sample,
    ym1: Sample,
    ym2: Sample,
    cutoffm1: Sample,
}


struct Context {
    left: PastSamples,
    right: PastSamples,
}

impl Context {
    fn new() -> Context {
        Context {
            left: PastSamples {
                xm1: 0.0,
                xm2: 0.0,
                ym1: 0.0,
                ym2: 0.0,
                cutoffm1: 0.0,
            },
            right: PastSamples {
                xm1: 0.0,
                xm2: 0.0,
                ym1: 0.0,
                ym2: 0.0,
                cutoffm1: 0.0,
            },
        }
    }

    fn next(&mut self, input: StereoSample, output: StereoSample, cutoff: StereoSample) {
        self.left.xm2 = self.left.xm1;
        self.left.ym2 = self.left.ym1;
        self.right.xm2 = self.right.xm1;
        self.right.ym2 = self.right.ym1;
        self.left.xm1 = input.left;
        self.right.xm1 = input.right;
        self.left.ym1 = output.left;
        self.right.ym1 = output.right;
        self.left.cutoffm1 = cutoff.left;
        self.right.cutoffm1 = cutoff.right;
    }
}

pub struct LowPass{
    context: HashMap<VoiceID, Context>
}

impl LowPass {
    pub fn new () -> LowPass {
        LowPass {
            context: HashMap::new(),
        }
    }

    pub fn name() -> &'static str {
        "Lowpass"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(LowPass::name(), Box::new(||{Box::from(LowPass::new())}));
    }

    // https://www.w3.org/2011/audio/audio-eq-cookbook.html
    fn filter(x: Sample, xm1: Sample, xm2: Sample, ym1: Sample, ym2: Sample, cutoff: Sample) -> Sample {
        let pi = PI as Sample;
        let two = 2.0 as Sample;

        let cutoff = cutoff + Sample::EPSILON;

        let sample_rate = 48000.0;
        let bw = 1.0;

        let w0 = 2.0*pi*cutoff/sample_rate;
        let alpha = (w0.sin())*((bw*(w0/w0.sin())*(two.ln()/2.0)).sinh());

        let b0 = (1.0-w0.cos())/2.0;
        let b1 =  1.0-w0.cos();
        let b2 = (1.0-w0.cos())/2.0;
        let a0 = 1.0+alpha;
        let a1 = -2.0*w0.cos();
        let a2 = 1.0-alpha;

        let y = x*b0/a0 + xm1*b1/a0 + xm2*b2/a0 - ym1*a1/a0 - ym2*a2/a0;
        y
    }

    fn update_sample(context: &mut Context,
                     inputs: &Vec<StereoSample>,
                     _: &VoiceID) -> Vec<StereoSample> {
        tracy_scope!();
        let input = inputs[0];
        let cutoff = inputs[1];
        let left = LowPass::filter(input.left,context.left.xm1, context.left.xm2,context.left.ym1,context.left.ym2, cutoff.left);
        let right = LowPass::filter(input.right,context.right.xm1, context.right.xm2,context.right.ym1,context.right.ym2, cutoff.right);

        let output = StereoSample::new(left,right);

        context.next(input, output, cutoff);

        vec![StereoSample::new(left,right)]
    }
}

impl Module for LowPass {
    fn name(&self) -> &'static str {
        LowPass::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["input", "cutoff"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["output"]
    }

    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     inputs_buffers: &Vec<StereoBuffer>,
                     voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let n_outputs = self.outputs().len();
        let mut output_buffers = Vec::with_capacity(n_outputs);
        for _ in 0..n_outputs {
            output_buffers.push(context.store.allocate_stereo(context.length));
        }
        let lowpass_context = self.context.get_mut(voice_id).unwrap();
        for i in 0..context.length {
            tracy_scope_named!("sample");
            let mut inputs = Vec::with_capacity(inputs_buffers.len());
            for input_buffer in inputs_buffers {
                inputs.push(input_buffer.at(i))
            }
            let outputs = LowPass::update_sample(lowpass_context, &inputs, voice_id);
            for o in 0..outputs.len() {
                output_buffers[o].set(i,outputs[o]);
            }
        }
        output_buffers
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut unused_voices = HashSet::new();
        let silence = StereoBuffer::constant(0.0);
        for voice in self.context.keys() {
            unused_voices.insert(*voice);
        }
        let mut output_voices = Voices::new(self.outputs().len());

        for (voice_id, input_buffers) in inputs.deref() {
            if !self.context.contains_key(voice_id) {
                self.context.insert(*voice_id, Context::new());
            }
            unused_voices.remove(voice_id);

            let output_buffers = self.update_buffer(context, input_buffers, voice_id);
            output_voices.insert(*voice_id, output_buffers);
        }

        for voice_id in unused_voices {
            let cutoff = self.context.get_mut(&voice_id).unwrap().left.cutoffm1;
            let output_buffers = self.update_buffer(context, &vec![silence,StereoBuffer::constant(cutoff)], &voice_id);
            output_voices.insert(voice_id, output_buffers);
            self.context.remove(&voice_id);
        }
        output_voices
    }
}