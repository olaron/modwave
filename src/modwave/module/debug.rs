use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Registry, StereoSample, UpdateContext};
use crate::modwave::module::Voices;
use std::ops::Deref;
use crate::c::nullptr;

struct DebugInput {
    voice_id: VoiceID,
    value: StereoSample,
}

pub struct Debug{
    inputs: Vec<DebugInput>
}

impl Debug {
    pub fn new() -> Debug {
        Debug {
            inputs: vec![],
        }
    }

    pub fn name() -> &'static str {
        "Debug"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Debug::name(), Box::new(||{Box::from(Debug::new())}));
    }
}

impl Module for Debug {
    fn name(&self) -> &'static str {
        Debug::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["input"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec![]
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        self.inputs.clear();
        for (voice_id, input_buffers) in inputs.deref() {
            let input = DebugInput{
                voice_id: *voice_id,
                value: input_buffers[0].at(context.length-1),
            };
            self.inputs.push(input);
        }
        Voices::new(0)
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        self.inputs.sort_by(|a, b| a.voice_id.partial_cmp(&b.voice_id).unwrap());
        unsafe {
            cimgui::igText(cstr!("VoiceID   "));
            cimgui::igSameLine(0.0,-1.0);
            cimgui::igText(cstr!("Left      "));
            cimgui::igSameLine(0.0,-1.0);
            cimgui::igText(cstr!("Right     "));
            for input in &self.inputs {
                cimgui::igText(cstr!("%-10d"), input.voice_id);
                cimgui::igSameLine(0.0,-1.0);
                cimgui::igText(cstr!("%-10.6f"),input.value.left);
                cimgui::igSameLine(0.0,-1.0);
                cimgui::igText(cstr!("%-10.6f"),input.value.right);
            }
            cimgui::igColumns(1, nullptr(), true);
        }
    }
}