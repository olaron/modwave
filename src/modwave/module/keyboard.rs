use std::collections::{HashMap};
use super::{VoiceID, ModuleIOName, new_id, Module};
use crate::modwave::{Registry, StereoBuffer, Sample, UpdateContext};
use sdl2_sys::SDL_Scancode;
use crate::modwave::module::Voices;
use crate::imgui;

pub struct Keyboard {
    keymap: HashMap<i32, Sample>,
    pressed_notes: HashMap<i32, VoiceID>,
}

impl Module for Keyboard {
    fn name(&self) -> &'static str {
        Keyboard::name()
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec![]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["note"]
    }

    fn update_voices(&mut self,
                     _: &mut UpdateContext,
                     _: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut output_voices = Voices::new(1);
        for (scancode, voice_id) in &self.pressed_notes {
            let note = self.keymap.get(&scancode).unwrap();
            output_voices.insert(*voice_id, vec![StereoBuffer::constant(*note)]);
        }
        output_voices
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        let io = imgui::get_io();
        if unsafe{!(*io.io).WantTextInput} {
            for (key, _) in &self.keymap {
                update_key_down(&mut self.pressed_notes, *key, unsafe{cimgui::igIsKeyDown(*key)});
            }
        }
    }
}

impl Keyboard {
    pub fn new() -> Keyboard {
        let mut keymap = HashMap::new();
        keymap.insert(SDL_Scancode::SDL_SCANCODE_A as i32, 0.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_Z as i32, 1.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_S as i32, 2.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_X as i32, 3.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_D as i32, 4.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_C as i32, 5.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_F as i32, 6.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_V as i32, 7.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_G as i32, 8.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_B as i32, 9.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_H as i32, 10.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_N as i32, 11.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_J as i32, 12.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_M as i32, 13.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_K as i32, 14.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_COMMA as i32, 15.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_L as i32, 16.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_PERIOD as i32, 17.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_SEMICOLON as i32, 18.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_SLASH as i32, 19.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_APOSTROPHE as i32, 20.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_1 as i32, 10.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_Q as i32, 11.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_2 as i32, 12.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_W as i32, 13.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_3 as i32, 14.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_E as i32, 15.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_4 as i32, 16.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_R as i32, 17.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_5 as i32, 18.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_T as i32, 19.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_6 as i32, 20.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_Y as i32, 21.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_7 as i32, 22.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_U as i32, 23.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_8 as i32, 24.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_I as i32, 25.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_9 as i32, 26.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_O as i32, 27.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_0 as i32, 28.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_P as i32, 29.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_MINUS as i32, 30.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_LEFTBRACKET as i32, 31.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_EQUALS as i32, 32.0);
        keymap.insert(SDL_Scancode::SDL_SCANCODE_RIGHTBRACKET as i32, 33.0);
        Keyboard {
            keymap,
            pressed_notes: HashMap::new()
        }
    }

    pub fn name() -> &'static str {
        "Keyboard"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Keyboard::name(), Box::new(||Box::from(Keyboard::new())));
    }
}

pub fn update_key_down(pressed_notes: &mut HashMap<i32, VoiceID>, key: i32, is_down: bool) {
    if is_down && !pressed_notes.contains_key(&key) {
        //println!("Key {:?} pressed", key);
        pressed_notes.insert(key, new_id());
    }
    else if !is_down && pressed_notes.contains_key(&key) {
        //println!("Key {:?} released", key);
        pressed_notes.remove(&key);
    }
}