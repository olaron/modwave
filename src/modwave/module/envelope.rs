use std::collections::{HashMap, HashSet};
use super::{VoiceID, ModuleIOName, Module};
use crate::modwave::{Sample, Registry, StereoBuffer, UpdateContext, StereoSample};
use crate::modwave::module::Voices;
use std::ops::Deref;

#[derive(PartialEq)]
enum Step {
    ATTACK,
    DECAY,
    SUSTAIN,
    RELEASE,
    END,
}

struct EnvelopeContext {
    step: Step,
    time: usize,
    note: StereoSample,
    output: Sample,
}

impl EnvelopeContext {
    fn new() -> EnvelopeContext {
        EnvelopeContext {
            step: Step::ATTACK,
            time: 0,
            note: StereoSample::from_sample(0.0),
            output: 0.0,
        }
    }
}

pub struct Envelope {
    context: HashMap<VoiceID, EnvelopeContext>,
    attack: Sample,
    decay: Sample,
    sustain: Sample,
    release: Sample,
}

impl Envelope {
    pub fn new() -> Envelope {
        Envelope {
            context: HashMap::new(),
            attack: 0.0,
            decay: 1.0,
            sustain: 1.0,
            release: 0.0
        }
    }

    pub fn name() -> &'static str {
        "Envelope"
    }

    pub fn register(registry: &mut Registry) {
        registry.register_module(Envelope::name(), Box::new(||{Box::from(Envelope::new())}));
    }

    fn update_buffer_released(&mut self,
                     context: &mut UpdateContext,
                     voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let sample_rate = context.sample_rate as Sample;
        let mut result = context.store.allocate(context.length);
        let env_context = self.context.get_mut(voice_id).unwrap();
        if env_context.step != Step::RELEASE && env_context.step != Step::END {
            env_context.step = Step::RELEASE;
            env_context.time = 0;
        }
        for i in 0..context.length {
            tracy_scope_named!("sample");
            let time = env_context.time as Sample / sample_rate;
            if env_context.step == Step::RELEASE {
                result[i] = lerp(env_context.output, (time as Sample / self.release).min(1.0).max(0.0), 0.0);
                if time > self.release {
                    env_context.step = Step::END;
                }
                else {
                    env_context.time += 1;
                }
            } else if env_context.step == Step::END {
                result[i] = 0.0;
            }
        }
        vec![StereoBuffer::constant(env_context.note.left), StereoBuffer::new(result,result)]
    }
}

fn lerp(a: Sample, x: Sample, b: Sample) -> Sample {
    a * (1.0 - x) + b * x
}

impl Module for Envelope {
    fn name(&self) -> &'static str {
        "Envelope"
    }

    fn inputs(&self) -> Vec<&ModuleIOName> {
        vec!["note"]
    }

    fn outputs(&self) -> Vec<&ModuleIOName> {
        vec!["note", "envelope"]
    }

    fn update_buffer(&mut self,
                     context: &mut UpdateContext,
                     inputs_buffers: &Vec<StereoBuffer>,
                     voice_id: &VoiceID) -> Vec<StereoBuffer> {
        tracy_scope!();
        let sample_rate = context.sample_rate as Sample;
        let note = inputs_buffers[0];
        let mut result = context.store.allocate(context.length);
        if !self.context.contains_key(voice_id) {
            self.context.insert(*voice_id, EnvelopeContext::new());
        }
        let env_context = self.context.get_mut(voice_id).unwrap();

        for i in 0..context.length {
            tracy_scope_named!("sample");
            let time = env_context.time as Sample / sample_rate;
            match env_context.step {
                Step::ATTACK => {
                    result[i] = (time / self.attack).min(1.0).max(0.0);
                    if time > self.attack {
                        env_context.step = Step::DECAY;
                        env_context.time = 0;
                    }
                    else {
                        env_context.time += 1;
                    }
                }
                Step::DECAY => {
                    result[i] = lerp(1.0, (time as Sample / self.decay).min(1.0).max(0.0), self.sustain);
                    if time > self.decay {
                        env_context.step = Step::SUSTAIN;
                        env_context.time = 0;
                    }
                    else {
                        env_context.time += 1;
                    }
                }
                Step::SUSTAIN => {
                    result[i] = self.sustain
                }
                Step::RELEASE => {unreachable!()}
                Step::END => {unreachable!()}
            }
        }
        env_context.note = note.at(context.length - 1);
        env_context.output = result[context.length - 1];
        vec![note, StereoBuffer::new(result,result)]
    }

    fn update_voices(&mut self,
                     context: &mut UpdateContext,
                     inputs: &Voices) -> Voices {
        tracy_scope_named!(self.name());
        let mut unused_voices = HashSet::new();
        for voice in self.context.keys() {
            unused_voices.insert(*voice);
        }

        let mut output_voices = Voices::new(self.outputs().len());
        for (voice_id, input_buffers) in inputs.deref() {
            unused_voices.remove(voice_id);
            let output_buffers = self.update_buffer(context, input_buffers, voice_id);
            output_voices.insert(*voice_id, output_buffers);
        }

        for voice in unused_voices {
            let env_context = self.context.get(&voice).unwrap();
            if env_context.step == Step::END {
                self.context.remove(&voice);
            }
            else {
                let output_buffers = self.update_buffer_released(context,&voice);
                output_voices.insert(voice, output_buffers);
            }
        }
        output_voices
    }

    fn draw_gui(&mut self) {
        tracy_scope!();
        unsafe {
            let mut a = self.attack as f32;
            cimgui::igDragFloat(cstr!("A"), &mut a, 0.01, 0.0, 100.0, cstr!("%.3f"), 0);
            self.attack = a as Sample;

            let mut d = self.decay as f32;
            cimgui::igDragFloat(cstr!("D"), &mut d, 0.01, 0.0, 100.0, cstr!("%.3f"), 0);
            self.decay = d as Sample;

            let mut s = self.sustain as f32;
            cimgui::igDragFloat(cstr!("S"), &mut s, 0.01, 0.0, 1.0, cstr!("%.3f"), 0);
            self.sustain = s as Sample;

            let mut r = self.release as f32;
            cimgui::igDragFloat(cstr!("R"), &mut r, 0.01, 0.0, 100.0, cstr!("%.3f"), 0);
            self.release = r as Sample;
        }
    }
}
