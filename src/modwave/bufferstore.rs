use std::ops::{Index, IndexMut};

pub type Sample = f64;

#[derive(Clone, Copy, Debug)]
struct SliceInfo {
    start: usize,
    length: usize,
}

#[derive(Clone, Copy, Debug)]
pub struct StereoSample {
    pub left: Sample,
    pub right: Sample,
}

impl StereoSample {
    pub fn new(left: Sample, right: Sample) -> StereoSample {
        StereoSample {
            left,
            right,
        }
    }
    pub fn from_sample(sample: Sample) -> StereoSample {
        StereoSample {
            left: sample,
            right: sample,
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct StereoBuffer {
    pub left: Buffer,
    pub right: Buffer,
}

impl StereoBuffer {
    pub fn new(left: Buffer, right: Buffer) -> StereoBuffer {
        StereoBuffer { left, right }
    }

    pub fn constant(value: Sample) -> StereoBuffer {
        StereoBuffer {
            left: Buffer::Constant(value),
            right: Buffer::Constant(value),
        }
    }

    pub fn set(&mut self, index: usize, sample: StereoSample) {
        self.left[index] = sample.left;
        self.right[index] = sample.right;
    }

    pub fn at(&self, index: usize) -> StereoSample {
        StereoSample {
            left: self.left[index],
            right: self.right[index],
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Buffer {
    View(BufferView),
    Constant(Sample),
}

#[derive(Clone, Copy, Debug)]
pub struct BufferView {
    vec_ptr: *mut Vec<Sample>,
    slice: SliceInfo,
}

impl Index<usize> for Buffer {
    type Output = Sample;

    fn index(&self, index: usize) -> &Self::Output {
        match self {
            Buffer::View(view) => {
                debug_assert!(!view.vec_ptr.is_null());
                debug_assert!(index < view.slice.length);
                debug_assert!(unsafe {view.vec_ptr.as_ref().unwrap().len() > index + view.slice.start});
                unsafe { &(&*view.vec_ptr)[index + view.slice.start] }
            }
            Buffer::Constant(s) => {
                s
            }
        }
    }
}

impl IndexMut<usize> for Buffer {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        match self {
            Buffer::View(view) => {
                debug_assert!(!view.vec_ptr.is_null());
                debug_assert!(index < view.slice.length);
                debug_assert!(unsafe {view.vec_ptr.as_ref().unwrap().len() > index + view.slice.start});
                unsafe { &mut (&mut *view.vec_ptr)[index + view.slice.start] }
            }
            Buffer::Constant(_) => {
                panic!()
            }
        }
    }
}

pub struct BufferStore {
    memory: Vec<Sample>,
}


impl BufferStore {
    pub fn new() -> BufferStore {
        BufferStore {
            memory: Vec::new()
        }
    }

    pub fn allocate(&mut self, length: usize) -> Buffer {
        tracy_scope!();
        debug_assert!(length > 0);
        let current_len = self.memory.len();
        let new_len = current_len + length;
        if self.memory.capacity() < new_len {
            self.memory.reserve(length);
        }
        unsafe {self.memory.set_len(new_len);}
        Buffer::View(BufferView {
            vec_ptr: &mut self.memory,
            slice: SliceInfo {
                start: current_len,
                length,
            }
        })
    }

    pub fn allocate_stereo(&mut self, length: usize) -> StereoBuffer {
        tracy_scope!();
        let left = self.allocate(length);
        let right = self.allocate(length);
        StereoBuffer::new(left,right)
    }

    pub fn clear(&mut self) {
        self.memory.clear();
    }
}