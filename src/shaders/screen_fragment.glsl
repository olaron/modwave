#version 430 core
out vec4 FragColor;
in vec2 TexCoords;
uniform sampler2D screenTexture;
uniform vec2 screenSize;
uniform float time;

float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 pick(vec2 pos) {
    vec4 color = texture(screenTexture, pos);
    if ((color.r + color.g + color.b)/3 < 0.2 && rand(vec2(pos.y) * time) < 0.1)
    {
        float r = rand(pos * time)*15;
        for(int i = 0; i < r-1; ++i) {
            color = (1- pow(0.4,(i/2)+1)) * color + pow(0.4,(i/2)+1) * texture(screenTexture, pos - vec2(((((tan(pos.y*5 + time/2)+1)/2)*10)+10) * (i+1), 0.0)/screenSize);
        }
    }
    return color;
}

vec4 invert(vec4 color) {
    return vec4(vec3(1.0 - color), 1.0);
}

void main()
{
    FragColor = texture(screenTexture, TexCoords);
}

