#[allow(unused_macros)]
macro_rules! function {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);
        &name[..name.len() - 3]
    }}
}

#[cfg(feature = "tracy")]
macro_rules! tracy_scope_named {
    ($name: expr) => {
        use tracy_client::Span;
        let _tscope = Span::new($name, function!(), file!(), line!(),0);
    }
}

#[cfg(not(feature = "tracy"))]
macro_rules! tracy_scope_named {
    ($name: expr) => {}
}

#[cfg(feature = "tracy")]
macro_rules! tracy_scope {
    () => {
        use tracy_client::Span;
        let _tscope = Span::new(function!(), function!(), file!(), line!(),0);
    }
}

#[cfg(not(feature = "tracy"))]
macro_rules! tracy_scope {
    () => {}
}

#[cfg(feature = "tracy")]
macro_rules! tracy_frame {
    () => {
        tracy_client::finish_continuous_frame!();
    }
}

#[cfg(not(feature = "tracy"))]
macro_rules! tracy_frame {
    () => {}
}