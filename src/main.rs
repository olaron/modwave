#[macro_use]
mod c;

#[macro_use]
mod tracy;

mod modwave;
use modwave::module::*;
use modwave::*;

mod sdl2_usr;
mod imgui;
mod imgui_impl_sdl;
mod imgui_impl_gl3;

use std::time::{Instant};
use cimgui::ImVec2;


struct Model {
    quit: bool,
    start_instant: Instant,
    program_gui: Program,
    update_context: UpdateContext,
    demo: bool,
}

impl Model {
    fn new(program_gui: Program) -> Model {
        Model {
            quit: false,
            start_instant: Instant::now(),
            program_gui,
            update_context: UpdateContext::new(256, 48000),
            demo: true,
        }
    }
}

pub struct Context {
    audio: sdl2_usr::AudioDevice,
    window: sdl2_usr::Window,
}

pub struct Game{
    model: Model,
    context: Context
}

impl Game {

    pub fn init() -> Result<Game, String> {
        tracy_scope!();
        let mut program_gui = Program::new();

        let keyboard_id = program_gui.add(Box::from(Keyboard::new()), ImVec2::new(0.0,50.0));
        let frequency_id = program_gui.add(Box::from(Frequency {}), ImVec2::new(150.0, 50.0));
        program_gui.connect(&Connection::new2(keyboard_id, 0, frequency_id, 0));
        let pulse_id = program_gui.add(Box::from(Pulse::new()), ImVec2::new(300.0, 50.0));
        program_gui.connect(&Connection::new2(frequency_id, 0, pulse_id, 0));

        let lp_id = program_gui.add(Box::from(LowPass::new()), ImVec2::new(450.0, 100.0));
        program_gui.connect(&Connection::new2(pulse_id, 0, lp_id,0));
        program_gui.connect(&Connection::new2(frequency_id, 0, lp_id,1));


        let value_id = program_gui.add(Box::from(Value::new(1.0)), ImVec2::new(400.0, 150.0));
        let gain_id = program_gui.add(Box::from(Operator::new("*")), ImVec2::new(600.0, 100.0));
        program_gui.connect(&Connection::new2(lp_id, 0, gain_id,0));
        program_gui.connect(&Connection::new2(value_id, 0, gain_id,1));

        program_gui.add_output("output");
        program_gui.connect(&Connection::new2(gain_id, 0, OUTPUT_MODULE_ID,0));


        let model = Model::new(program_gui);

        let context = create_context()?;
        Ok(Game {
            model,
            context
        })
    }

    pub fn run(&mut self) {
        tracy_scope!();
        loop{
            if self.model.quit {
                break;
            }
            audio_update(&mut self.model, &mut self.context.audio);
            screen_update(&mut self.model, &mut self.context.window);
        }
    }
}

fn create_context() -> Result<Context, String>{

    sdl2_usr::init();
    sdl2_usr::init_gl_attributes();
    let window = sdl2_usr::Window::create();
    {
        let index = sdl2_usr::find_driver("opengl");
        if index < 0 {
            panic!("No OpenGL driver.");
        }
        let context = window.gl_create_context();
        sdl2_usr::set_swap_interval(1);
        window.gl_make_current(&context);
        gl::load_with(|name| {
            sdl2_usr::gl_get_proc_address(name) as *const _
        });
        imgui::check_version();
        imgui::create_context();
        imgui::style_colors_dark();
        imgui_impl_sdl::imgui_init_sdl2(&window);
        imgui_impl_gl3::imgui_init_gl3();
    }

    let audio = sdl2_usr::open_audio_device();


    println!("Using audio driver: {}; Device: {}", sdl2_usr::current_audio_driver().unwrap(), sdl2_usr::audio_playback_device_name(0));


    Ok(Context {
        audio,
        window,
    })
}

fn audio_update(model: &mut Model, audio: &mut sdl2_usr::AudioDevice) {
    tracy_scope!();
    let queue_size = audio.size() / 4;
    let buffer_length = audio.audio_spec.samples;
    let target_queue_length = buffer_length * 16;
    if queue_size < target_queue_length as u32 {
        if queue_size == 0 {
            println!("empty queue!");
        }
        let length = target_queue_length as u32 - queue_size;
        model.update_context.length = length as usize;
        let output = model.program_gui.update_voices(&mut model.update_context, &Voices::new(0)).to_16bit_stereo(model.update_context.length);
        audio.queue(&output);
        if audio.status() != sdl2_sys::SDL_AudioStatus::SDL_AUDIO_PLAYING {
            audio.resume();
        }
        model.update_context.store.clear();
    }
}

fn screen_update(model: &mut Model, window: &mut sdl2_usr::Window) {
    tracy_scope!();
    unsafe {
        while let Some(event) = sdl2_usr::poll_event() {
            tracy_scope_named!("event");
            imgui_impl_sdl::process_event(event);
            if event.type_ == sdl2_sys::SDL_EventType::SDL_QUIT as u32 {
                model.quit = true;
            }
            if event.type_ == sdl2_sys::SDL_EventType::SDL_WINDOWEVENT as u32 && event.window.event == sdl2_sys::SDL_WindowEventID::SDL_WINDOWEVENT_CLOSE as u8 && event.window.windowID == sdl2_sys::SDL_GetWindowID(window.window) {
                model.quit = true;
            }
            if event.type_ == sdl2_sys::SDL_EventType::SDL_WINDOWEVENT as u32 && event.window.event == sdl2_sys::SDL_WindowEventID::SDL_WINDOWEVENT_SIZE_CHANGED as u8 {
                imgui_impl_gl3::window_resize(event.window.data1, event.window.data2);
            }
        }

        {
            tracy_scope_named!("new frame");
            imgui_impl_sdl::new_frame(window.window);

            cimgui::igNewFrame();
        }

        if model.demo {
            tracy_scope_named!("demo window");
            cimgui::igShowDemoWindow(&mut model.demo);
        }

        model.program_gui.window_opened = true;
        model.program_gui.draw_window();

        {
            tracy_scope_named!("render");
            cimgui::igRender();
            imgui_impl_gl3::render_screen(model.start_instant.elapsed().as_secs_f32());
            {
                tracy_scope_named!("swap window");
                sdl2_sys::SDL_GL_SwapWindow(window.window);
            }
        }
        tracy_frame!();
    }
}


fn register_modules() {
    tracy_scope!();
    unsafe {
        REGISTRY = Some(Registry::new());
        let registry = REGISTRY.as_mut().unwrap();
        Frequency::register(registry);
        Keyboard::register(registry);
        LowPass::register(registry);
        Pulse::register(registry);
        Value::register(registry);
        Debug::register(registry);
        Formula::register(registry);
        Time::register(registry);
        Program::register(registry);
        Oscilloscope::register(registry);
        Envelope::register(registry);
        Sequencer::register(registry);
        Operator::register(registry);
    }
}

pub fn main() -> Result<(), String> {
    register_modules();
    let mut game = Game::init()?;
    game.run();
    Ok(())
}
