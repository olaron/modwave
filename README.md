# modwave.

Modular realtime audio processing

Interactive demo available [here](https://zblorg.xyz/modwave/) (works best on Firefox, sound might be 'crunchy' on Chrome)

## Requirements

[Rust](https://www.rust-lang.org/learn/get-started)

[Clang](https://rust-lang.github.io/rust-bindgen/requirements.html) for bindgen: 

CMake for [cmake-rs](https://github.com/alexcrichton/cmake-rs)

## Setup

Clone the repository and their submodules recursively
```
git clone --recurse-submodules <URL>
```

Build
```
cargo build --release
```

Run
```
cargo run --release
```

## TODO

### Short term
* Save and load programs
* Basic modules
    * Basic waveform oscillators
        - [ ] Sine
        - [x] Square
        - [ ] Triangle
        - [ ] Sawtooth
    * Basic filters
        - [ ] Highpass
        - [x] Lowpass
* Piano roll sequencer module
* Delay module

### Mid term
* Button to disable modules
* Sampler module
* Audio recording module
* Bitcrush module
* Module documentation tooltip
* Audio rendering
* Area module selection
* Copy, cut and paste modules
* Reverb module
* ASIO driver support
* Audio input
* Multi-thread audio processing
* UI rendering on a separate thread
* MIDI input/output support
* Custom GUI for program modules

### Long term
* Try out another language (Zig or Jai...)
* Make it a library usable for video games
* Make it a VST plugin
