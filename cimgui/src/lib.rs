#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use std::ops::{Add, Sub};

impl ImVec2 {
    pub fn new(x: f32, y:f32) -> ImVec2 {
        ImVec2{x,y}
    }

    pub fn distance(&self, p: &ImVec2) -> f32 {
        ((self.x - p.x).powi(2) + (self.y - p.y).powi(2)).sqrt()
    }
}

impl Add for ImVec2 {
    type Output = ImVec2;

    fn add(self, rhs: Self) -> Self::Output {
        ImVec2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Sub for ImVec2 {
    type Output = ImVec2;

    fn sub(self, rhs: Self) -> Self::Output {
        ImVec2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
